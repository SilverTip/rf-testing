## Benjamin Stadnik Orbital Research Ltd.
## Stolen pieces from ODU-Test code
## Upgraded to Python 3.9.7

import serial, os, time ,csv
import pyvisa as visa
import time
from datetime import datetime
import numpy as np

RcodeGOOD = 0
RcodeFAIL = -1
RcodeQUIT = RcodeFAIL
def pause_anykey():
    data = input("***PAUSED - Press (Q)uit or any key to continue: ")
    if data == "q" or data == "Q":
        print("\r\n>>> TEST ABORTED\n\r")
        return(RcodeFAIL)
    return(RcodeGOOD)
##---------------------------------------------------------------------------
# Define Error Check Function
def Errcheck():
    myError = []
    ErrorList = N9918A.query("SYST:ERR?").split(',')
    Error = ErrorList[0]
    if int(Error) == 0:
        myError.append('0')
        myError.append("No error")
    else:
     while int(Error)!=0:
        print ("Error #: " + ErrorList[0])
        print ("Error Description: " + ErrorList[1])
        myError.append(ErrorList[0])
        myError.append(ErrorList[1])
        ErrorList = N9918A.query("SYST:ERR?").split(',')
        Error = ErrorList[0]
        myError = list(myError)
    return myError
##---------------------------------------------------------------------------
def setupFOX():
    global rm,N9918A,IPaddress
    defaultIP = "10.0.10.101"
    altIP = "10.0.10.131"
    defaultFF = "MY53101679"
    altFoxSN = "MY53101680"
    IPaddress = ''
    FoxSN = ''
    UserInput = input("FieldFox N9918A SN:[%s][0 to skip]: " %(defaultFF))
    if UserInput == '':
        FoxSN = defaultFF
    elif (UserInput != '0'):
        FoxSN = UserInput
    IPaddress = input("Enter FieldFox IP address [xxx.xxx.xxx.xxx(%s)]: " %(defaultIP))
    if IPaddress == '':
        IPaddress = defaultIP
    print ("using N9918A sn: " + FoxSN + " IP: " + IPaddress)
    rm = visa.ResourceManager()
    N9918A = rm.open_resource('TCPIP0::' +  str(IPaddress) + '::inst0::INSTR')
    N9918A.timeout = 50000
# Clear the event status registers and empty the error queue
    N9918A.write("*CLS")
# Query identification string *IDN?
    print (N9918A.query("*IDN?"))
# Call and print error check results
    print (Errcheck())
# Query capability
##    print(N9918A.query(':Inst:Cat?'))
##---------------------------------------------------------------------------
def take_screenshot(pictureFileName):
#saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
#fetching the image data in an array
    fetch_image_data = N9918A.query_binary_values('MMEMory:DATA? "TESTZ.png"',datatype='B',is_big_endian=False,container=bytearray)
#creating a file with the product number to write the image data to it
    save_dir = open(pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()
#deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")
##---------------------------------------------------------------------------
def getfilename(count,name):
    if (name == ''):
        defaultUID = "0000-000"
        CIDUID = input("enter CID-UID xxxx-xxx for file name:[default:[%s]:" %(defaultUID))
        if CIDUID == "":
            CIDUID=defaultUID
        subdir = CIDUID+ " RF Test Data\\"
        if not os.path.exists(subdir):
            os.makedirs(subdir)
        print(("#output subdirectory: %s" %(subdir)))       
        return(subdir + CIDUID + '_')
    else:
        filename = input("test:[%d], Enter Screenshot TEST name or (Q)uit: " %(count)).upper()
        if filename != "Q":
            filename = name + filename
            print(("#output file prefix: %s" %(filename))) 
        return(filename)      

def save_data(filename):
    N9918A.write("MMEM:STOR:FDAT 'MyFile.csv'")
    fetch_data = N9918A.query_binary_values('MMEMory:DATA? "MyFile.csv"',datatype='B',is_big_endian=False,container=bytearray)
    save_dir = open(filename+".csv", 'wb')
    save_dir.write(fetch_data)
    save_dir.close()
    N9918A.write(":MMEM:DEL 'MyFile.csv'")
    N9918A.write("*CLS")

def average_gain():
    N9918A.write('CALC:MATH:MEM')  
    gain_csv = N9918A.query('CALC:FMEM:DATA?')
    temp = ''
    for x in gain_csv:
        if x != ',':
            temp += x
        else:
            gain.append(float(temp))
            temp = ''
    #print(gain)
    averageGain = np.average(gain)
    return(averageGain)
    
##MAIN
APPtitle="FIELDFOX SCREENSHOT BOT"
APPver = str(os.path.basename(__file__))
print(("%s, (%s)" %(APPtitle, APPver)))
print('-------------------------------------------')
     

setupFOX()

print('Connected to FieldFox ' + str(IPaddress))  

count = 1
unit = getfilename(count,'')

while(True):
    gain = []
    FFmode = "?"
    N9918A.write(':INST:SEL?')
    FFxmode = str(N9918A.read_raw())
##    print (FFxmode)
    if FFxmode.find('Power Meter') != -1:
        FFmode = "PM"
    elif FFxmode.find('NA') != -1:
        FFmode = "NA"
    elif FFxmode.find('SA') != -1:
        FFmode = "SA"  
##    print(FFmode)
    Ntraces = 1
    if (FFmode == "NA"):
        Ntraces = int(N9918A.query_ascii_values(':CALC:PARameter:COUNt?')[0])
    print("FF MODE: " + FFmode +', '+ str(Ntraces)+"xTrace(s)")
    filename = getfilename(count,unit)
    print (filename)
    if filename == "Q":
        break
    
    count = count + 1

    if (FFmode == "NA"):
        N9918A.write('CALC:PAR1:SEL')
        N9918A.write(':CALC:SEL:MARK1:FUNC:MAX')
        N9918A.write(':CALC:SEL:MARK2:FUNC:MIN')
        maximum = N9918A.query_ascii_values(':CALC:SEL:MARK1:Y?')
        minimum = N9918A.query_ascii_values(':CALC:SEL:MARK2:Y?')
        print("Trace1")
        print("Max: "+ str(maximum[0])) 
        print("Min: "+ str(minimum[0]))
        if Ntraces > 1:
            N9918A.write('CALC:PAR2:SEL')
            N9918A.write(':CALC:SEL:MARK1:FUNC:MAX')
            N9918A.write(':CALC:SEL:MARK2:FUNC:MIN')
            maximum = N9918A.query_ascii_values(':CALC:SEL:MARK1:Y?')
            minimum = N9918A.query_ascii_values(':CALC:SEL:MARK2:Y?')
            print("Trace2")
            print("Max: "+ str(maximum[0])) 
            print("Min: "+ str(minimum[0])) 
            if Ntraces > 2:
                N9918A.write('CALC:PAR3:SEL')
                N9918A.write(':CALC:SEL:MARK1:FUNC:MAX')
                N9918A.write(':CALC:SEL:MARK2:FUNC:MIN')
                maximum = N9918A.query_ascii_values(':CALC:SEL:MARK1:Y?')
                minimum = N9918A.query_ascii_values(':CALC:SEL:MARK2:Y?')
                print("Trace3")
                print("Max: "+ str(maximum[0])) 
                print("Min: "+ str(minimum[0]))
    elif (FFmode == "SA"):
        print("SA mode")
        N9918A.write(':CALC:MARK1:FUNC:MAX')
        N9918A.write(':CALC:MARK2:FUNC:MIN')
      
        print ("wrote")
        maximum = N9918A.query_ascii_values(':CALC:MARK1:Y?')
        minimum = N9918A.query_ascii_values(':CALC:MARK2:Y?')
        print("Trace1")
        print("Max: "+ str(maximum[0])) 
        print("Min: "+ str(minimum[0]))
 
    elif (FFmode == "PM"):
        print("Average: "+ str(average_gain())+'\n')
 
    time.sleep(0.5)
    print("screen")
    take_screenshot(filename)
    print("data")
    save_data(filename)
    print('\n')
##    if (pause_anykey()== RcodeFAIL):
##        break
print("Good-Bye")

#Version Log
#Revision 3 - 20210911 - added save_data function to gather raw data as csv
#r3-05 2021-10-12 min/max/avg dependant on FF mode, Ask for unit# and use as prefix
#r3-06 2021-10-?? Add subdir creation
#r3-07 2021-10-19 include SA mode and testname is made upper case

