#Redundant Systems ODU Datacapture - Fieldfox VNA
#Written by Benjamin Stadnik
#Orbital Research Ltd.

#Version Log
#---------------------------------------
#R1 - 2022-01-24 - Created first version from Fieldfox Screenshot Bot R2.py and Fieldfox Screenshot Bot R3_Gain.py

import os, time, csv
import pyvisa as visa
import datetime
import tkinter as tk
import numpy as np

version = 1

VNA_VISA = 'TCPIP0::10.0.10.131::inst0::INSTR'
 

def main():

    #Gain Test Function
    if(GainTest_EN.get()):
        gain = VNA.query('CALCulate[:SELected]:DATA:FDATa?')
        averageGain = np.mean(gain)

    #Return Loss Test Function
    if(RLTest_EN.get()):
        VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
        VNA.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
        maximum = VNA.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        minimum = VNA.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')

    #Record Screenshot
    if(PNG_EN.get()):
        time.sleep(0.5)
        Screenshot(ID.get())

    #Record .csv
    if(CSV_EN.get()):
        time.sleep(0.5)
        SaveCSV(ID.get())

    #Record Test Results in .txt file (backup to excel master doc)
    if(TXT_EN.get()):
        if not(os.path.exists(SN.get() + '.txt')):
            f = open(SN.get() + ' RF TEST.txt','w')
            f.write('INSERT TITLE HERE\n')
            f.write('INSERT DATE HERE\n')
            f.write(Tester.get()+'\n')
            f.write(SN.get()+'\n\n')
            f.close()
        f = open(SN.get() + '.txt','a')
        f.write(ID.get())
        if(GainTest_EN.get()):
            f.write(averageGain+'\n')
        if(RLTest_EN.get()):
            f.write(maximum+'\n')
            f.write(minimum+'\n')
        if(PNG_EN.get()):
            f.write('Screenshot name: ' + ID.get() + '.PNG\n')
        if(CSV_EN.get()):
            f.write('Screenshot name: ' + ID.get() + '.CSV\n')
        f.write('\n')
        f.close
                

    #Useful Commands
    #---------------------------
    #VNA.write('CALC:PAR1:SEL')
    #VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
    #VNA.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
    #gain = VNA.query('CALCulate[:SELected]:DATA:FDATa?')
    #averageGain = np.mean(gain)
    #VNA.write('CALC:PAR2:SEL')
    #VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
    #VNA.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
    #VNA.write('CALC:PAR3:SEL')
    #VNA.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
    #VNA.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
    #maximum = VNA.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
    #minimum = VNA.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
    #VNA.write('CALC:PAR1:SEL')

def setupFOX(VNA_VISA):
    global rm,VNA,IPaddress
    rm = visa.ResourceManager()
    VNA = rm.open_resource(VNA_VISA)
    VA.timeout = 50000
    print(VNA.query('*IDN?'))
    print(os.path.dirname(__file__))
        
def Screenshot(name):
    #saving a screenshot on the equipment                                 
    VNA.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = VNA.query_binary_values('MMEMory:DATA? "TESTZ.png"',datatype='B',is_big_endian=False,container=bytearray)

    #creating a file with the product number to write the image data to it
    save_dir = open(name +".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    VNA.write(":MMEM:DEL 'TESTZ.PNG'")
    VNA.write("*CLS")

def SaveCSV(filename):
    N9918A.write("MMEM:STOR:FDAT 'MyFile.csv'")
    fetch_data = N9918A.query_binary_values('MMEMory:DATA? "MyFile.csv"',datatype='B',is_big_endian=False,container=bytearray)
    save_dir = open(filename+".csv", 'wb')
    save_dir.write(fetch_data)
    save_dir.close()
    N9918A.write(":MMEM:DEL 'MyFile.csv'")
    N9918A.write("*CLS")

def RLDisable():
    RLTest_EN.set(not RLTest_EN.get())
def GainDisable():
    GainTest_EN.set(not GainTest_EN.get())

def GUI():
    global master
    master = tk.Tk() ## Initialize Menu variable
    master.title('RS FieldFox Data Capture Program R' + str(version))

    #GUI Variable list
    global GainTest_EN, RLTest_EN, PNG_EN, CSV_EN, TXT_EN, Tester, SN, ID
    GainTest_EN = tk.IntVar()
    RLTest_EN = tk.IntVar()
    PNG_EN = tk.IntVar()
    CSV_EN = tk.IntVar()
    TXT_EN = tk.IntVar()
    GainTest_EN.set(True)
    PNG_EN.set(True)
    CSV_EN.set(True)
    TXT_EN.set(True)

    #Test Menu
    TestFrame = tk.LabelFrame(master, text="Test Type:")
    TestFrame.grid(row=1,column=1,ipadx=134,ipady=5,sticky='W')
    tk.Checkbutton(TestFrame, text='Gain', variable=GainTest_EN, command=RLDisable).grid(row=1, column=1)
    tk.Checkbutton(TestFrame, text='Return Loss', variable=RLTest_EN, command=GainDisable).grid(row=1, column=2)

    #Data Menu
    DataFrame = tk.LabelFrame(master, text="Data Capture File Type:")
    DataFrame.grid(row=2,column=1,ipadx=126,ipady=5,sticky='W')
    tk.Checkbutton(DataFrame, text='.PNG', variable=PNG_EN).grid(row=1, column=1)
    tk.Checkbutton(DataFrame, text='.CSV', variable=CSV_EN).grid(row=1, column=2)
    tk.Checkbutton(DataFrame, text='.TXT', variable=TXT_EN).grid(row=1, column=3)
    
    #Information Menu
    Info = tk.LabelFrame(master, text="Unit Information Menu:")
    Info.grid(row=3,column=1,ipadx=100,ipady=5,sticky='W')

    #Serial Number
    tk.Label(Info, text='Serial Number:').grid(row=1, column=1)
    SN = tk.Entry(Info)
    SN.grid(row=1, column=2)

    #Tester Name
    tk.Label(Info, text='Tester:').grid(row=2, column=1,sticky='E')
    Tester = tk.Entry(Info)
    Tester.grid(row=2, column=2)

    #Test ID
    tk.Label(Info, text='Test ID:').grid(row=3, column=1,sticky='E')
    ID = tk.Entry(Info)
    ID.grid(row=3, column=2)
        
    #Button to Begin Testing
    Button1 = tk.Button(master,text='Capture Data',command=main).grid(row=4,column=1,columnspan=2,ipadx=100,ipady=10)

    master.mainloop()


#setupFOX(VNA_VISA)
GUI()   


