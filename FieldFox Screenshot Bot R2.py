## Benjamin Stadnik Orbital Research Ltd.
## Stolen pieces from ODU-Test code
## Upgraded to Python 3.9

import serial, os, time ,csv
import pyvisa as visa
import time
from datetime import datetime
#import numpy as np

def setupFOX():
    global rm,N9918A,IPaddress
    IPaddress = "10.0.10.131"
    rm = visa.ResourceManager()
    N9918A = rm.open_resource('TCPIP0::' +  str(IPaddress) + '::inst0::INSTR')
    N9918A.timeout = 50000
        
def take_screenshot(pictureFileName):

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_binary_values('MMEMory:DATA? "TESTZ.png"',datatype='B',is_big_endian=False,container=bytearray)

    #creating a file with the product number to write the image data to it
    save_dir = open(pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

##MAIN

print(str('FIELDFOX SCREENSHOT BOT V001'))
print(str('-------------------------------------------'))
     

setupFOX()

print('Connected to FieldFox ' + str(IPaddress))  

count = 1

while(True):
    pictureFileName = input("[%d] Screenshot file name: " %(count))
    count = count + 1
    #N9918A.write('CALC:PAR1:SEL')
    N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
    N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
    #gain = N9918A.query('CALCulate[:SELected]:DATA:FDATa?')
    #averageGain = np.mean(gain)
    #N9918A.write('CALC:PAR2:SEL')
    #N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
    #N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
    #N9918A.write('CALC:PAR3:SEL')
    #N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
    #N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
    #maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
    #minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
    #N9918A.write('CALC:PAR1:SEL')
    time.sleep(0.5)
    take_screenshot(pictureFileName)
    


