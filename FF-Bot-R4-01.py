## Orbital Research Ltd.
## Written by Doug Forsythe and Benjamin Stadnik
## 2022-04-25

import serial, os, time ,csv
import pyvisa as visa
import time
import datetime
import numpy as np
import tkinter as tk
import tkinter.font as tkFont
import math

FF_VISA = 'TCPIP0::10.0.10.120::inst0::INSTR'

version = 4

def main():
    gain = []
    maximum = ''
    minimum = ''
    averageGain = ''
    FFmode = "?"
    N9918A.write(':INST:SEL?')
    FFxmode = str(N9918A.read_raw())
    if FFxmode.find('Power Meter') != -1:
        FFmode = "PM"
    elif FFxmode.find('NA') != -1:
        FFmode = "NA"
    elif FFxmode.find('SA') != -1:
        FFmode = "SA"  
    Ntraces = 1  

    Display(F'Test: {str(ID.get())}')      

    if (FFmode == "NA"):
        Ntraces = int(N9918A.query_ascii_values(':CALC:PARameter:COUNt?')[0])
        N9918A.write('CALC:PAR1:SEL')
        N9918A.write(':CALC:SEL:MARK1:FUNC:MAX')
        N9918A.write(':CALC:SEL:MARK2:FUNC:MIN')
        maximum = N9918A.query_ascii_values(':CALC:SEL:MARK1:Y?')[0]
        minimum = N9918A.query_ascii_values(':CALC:SEL:MARK2:Y?')[0]
        Display(F'Trace 1:\nMaximum: {maximum}; Minimum: {minimum}')
        if Ntraces > 1:
            N9918A.write('CALC:PAR2:SEL')
            N9918A.write(':CALC:SEL:MARK1:FUNC:MAX')
            N9918A.write(':CALC:SEL:MARK2:FUNC:MIN')
            maximum = N9918A.query_ascii_values(':CALC:SEL:MARK1:Y?')[0]
            minimum = N9918A.query_ascii_values(':CALC:SEL:MARK2:Y?')[0]
            Display(F'Trace 2:\nMaximum: {maximum}; Minimum: {minimum}')
            if Ntraces > 2:
                N9918A.write('CALC:PAR3:SEL')
                N9918A.write(':CALC:SEL:MARK1:FUNC:MAX')
                N9918A.write(':CALC:SEL:MARK2:FUNC:MIN')
                maximum = N9918A.query_ascii_values(':CALC:SEL:MARK1:Y?')[0]
                minimum = N9918A.query_ascii_values(':CALC:SEL:MARK2:Y?')[0]
                Display(F'Trace 3:\nMaximum: {maximum}; Minimum: {minimum}')
    
    elif (FFmode == "SA"):
        N9918A.write(':CALC:MARK1:FUNC:MAX')
        N9918A.write(':CALC:MARK2:FUNC:MIN')
        maximum = N9918A.query_ascii_values(':CALC:MARK1:Y?')[0]
        minimum = N9918A.query_ascii_values(':CALC:MARK2:Y?')[0]
        Display(F'Maximum: {maximum}; Minimum: {minimum}')
 
    elif (FFmode == "PM"):
        average = round(average_gain(),2)
        Display(F'Average: {average}dB')

    #Screenshot    
    if(PNG_EN.get()):
        time.sleep(0.3)
        Screenshot(SN.get(),ID.get())

    #Record .csv
    if(CSV_EN.get()):
        time.sleep(0.3)
        SaveCSV(SN.get(),ID.get())

    #Record Test Results in .txt file (backup to excel master doc)
    if(TXT_EN.get()):
        if not(os.path.exists(F'{SN.get()} RF Results.txt')):
            f = open(F'{SN.get()} RF Results.txt','w')
            f.write(F'Unit ID: {str(SN.get())}\n')
            f.write(F'Time: {timestamp()[0]}\n')
            f.write(F'Tester: {str(Tester.get())}\n')
            f.write(F'Equipment: {N9918A.query("*IDN?")}\n\n')
            f.close()
        with open(F'{SN.get()} RF Results.txt','a') as f:
            f.write(F'Test: {ID.get()}\n')
            if(FFmode == 'NA' or FFmode == 'SA'):
                f.write(F'Maximum: {maximum}\n')
                f.write(F'Minimum: {minimum}\n')
            elif(FFmode == 'PM'):
                f.write(F'Average: {average} dB\n')
            if(PNG_EN.get()):
                f.write('Screenshot: ' + ID.get() + '.PNG\n')
            if(CSV_EN.get()):
                f.write('Raw Data: ' + ID.get() + '.CSV\n')
            f.write('\n')


def timestamp():
    now = {}
    current_time = datetime.datetime.now()
    now[0] = current_time.strftime("%Y %b %d %H:%M:%S")
    now[1] = current_time.strftime("%Y%b%d_%H%M%S")
    return now

def Errcheck(): #Define Error Check Function
    myError = []
    ErrorList = N9918A.query("SYST:ERR?").split(',')
    Error = ErrorList[0]
    if int(Error) == 0:
        myError.append('0')
        myError.append("No error")
    else:
     while int(Error)!=0:
        print ("Error #: " + ErrorList[0])
        print ("Error Description: " + ErrorList[1])
        myError.append(ErrorList[0])
        myError.append(ErrorList[1])
        ErrorList = N9918A.query("SYST:ERR?").split(',')
        Error = ErrorList[0]
        myError = list(myError)
    return myError

def setupFOX(FF_VISA):
    global rm, N9918A
    rm = visa.ResourceManager()
    N9918A = rm.open_resource(FF_VISA)
    N9918A.timeout = 10000  
    N9918A.write("*CLS") #Clear the event status registers and empty the error queue
    print (N9918A.query("*IDN?")) #Query identification string *IDN?
    print (Errcheck()) #Call and print error check results
    print('Connected to FieldFox ' + FF_VISA)     

def Screenshot(serial, name):
    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_binary_values('MMEMory:DATA? "TESTZ.png"',datatype='B',is_big_endian=False,container=bytearray)
    #creating a file with the product number to write the image data to it
    save_dir = open(serial + '_' + name +".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()
    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

def average_gain():
    N9918A.write('CALC:MATH:MEM')  
    gain_csv = N9918A.query('CALC:FMEM:DATA?')
    temp = ''
    lineargain = []
    for x in gain_csv:
        if x != ',':
            temp += x
        else:
            lineargain.append(10**(float(temp)/10))
            temp = ''
    
    averageLinearGain = np.average(lineargain)
    averageGain = 10 * math.log10(averageLinearGain)

    return(averageGain)
    
def SaveCSV(serial, filename):
    N9918A.write("MMEM:STOR:FDAT 'MyFile.csv'")
    fetch_data = N9918A.query_binary_values('MMEMory:DATA? "MyFile.csv"',datatype='B',is_big_endian=False,container=bytearray)
    save_dir = open(serial + '_' + filename+".csv", 'wb')
    save_dir.write(fetch_data)
    save_dir.close()
    N9918A.write(":MMEM:DEL 'MyFile.csv'")
    N9918A.write("*CLS")

def Display(message):
    mylist.insert(tk.END, message)
    mylist.see(tk.END)

def GUI():
    global master
    master = tk.Tk() ## Initialize Menu variable
    master.title(os.path.basename(__file__))
    #master.geometry(str(WIDTH)+'x'+str(HEIGHT))
    master.resizable(width=0, height=0)

    font = tkFont.Font(size=12)

    #GUI Variable list
    global PNG_EN, CSV_EN, TXT_EN, Tester, SN, ID
    PNG_EN = tk.IntVar()
    CSV_EN = tk.IntVar()
    TXT_EN = tk.IntVar()
    PNG_EN.set(True)
    CSV_EN.set(True)
    TXT_EN.set(True)

    #Data Menu
    DataFrame = tk.LabelFrame(master, text="Data Capture:",font=font)
    tk.Checkbutton(DataFrame, text='.PNG', variable=PNG_EN,font=font).grid(row=1, column=1)
    tk.Checkbutton(DataFrame, text='.CSV', variable=CSV_EN,font=font).grid(row=1, column=2)
    tk.Checkbutton(DataFrame, text='.TXT', variable=TXT_EN,font=font).grid(row=1, column=3)
    DataFrame.pack(fill='both')
    
    #Information Menu
    Info = tk.LabelFrame(master, text="Unit Information",font=font)
    tk.Label(Info, text='Serial Number:',font=font).grid(row=1, column=1)
    SN = tk.Entry(Info,font=font)
    SN.grid(row=1, column=2)
    tk.Label(Info, text='Tester:',font=font).grid(row=2, column=1,sticky='E')
    Tester = tk.Entry(Info,font=font)
    Tester.grid(row=2, column=2)
    tk.Label(Info, text='Test ID:',font=font).grid(row=3, column=1,sticky='E')
    ID = tk.Entry(Info,font=font)
    ID.grid(row=3, column=2)      
    Info.pack(fill='both')

    Button1 = tk.Button(master,text='Capture Data',command=main,height=2,font=font,relief='raised',bd=5)
    Button1.pack(fill='both',padx=5,pady=5)

    #List Frame
    ListFrame = tk.LabelFrame(master, text="",font=font)
    scrollbar = tk.Scrollbar(ListFrame)
    scrollbar.pack( side ='right', fill ='y' )
    global mylist
    mylist = tk.Listbox(ListFrame, yscrollcommand=scrollbar.set )
    mylist.pack(fill='both')
    scrollbar.config(command=mylist.yview)
    ListFrame.pack(fill='both')

    master.mainloop()


setupFOX(FF_VISA)
GUI()   


#Version Log
#Revision 3 - 20210911 - added save_data function to gather raw data as csv
#r3-05 2021-10-12 min/max/avg dependant on FF mode, Ask for unit# and use as prefix
#r3-06 2021-10-?? Add subdir creation
#r3-07 2021-10-19 include SA mode and testname is made upper case
#R4-01 2022-04-25 - Include GUI to remove text-line interface
