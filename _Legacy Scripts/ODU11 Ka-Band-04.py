## Federico / Doug Forsythe Orbital Research Ltd.
##-----------------------------------------------------------
import serial, os, time ,csv
import pyvisa as visa
import time
from datetime import datetime

##-----------------------------------------------------------
## From various code from Federico
##2020-07-13 thus begins the rewrite for Ka 1:1
##-----------------------------------------------------------
## Functions:

##def take_screenshot():
##def activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfile,isVSWR,PwrCal,Comp,Expect):
##def activateMaxMinM20C(N9918A,InputLevel,ODU_SN,TestNum,logfile,isVSWR,PwrCal,Comp,Expect):
##def activateMaxMinM30C(N9918A,InputLevel,ODU_SN,TestNum,logfile,isVSWR,PwrCal,Comp,Expect):
##-----------------------------------------------------------

SilentRunning = 0 ##1 = use defaults and no pause for key 0= normal
#Strings and setup basic commands
IS_SER = 0      #non-intialized value
IS_LOG = 0      #non-intialized value
UTC = 0         #non-intialized value
defaultCID = 'T999'    #non-intialized value
defaultUID = '101'    #non-intialized value
defaultBAND = 'Ku'
defaultCFG = '1:1'
defaultPLL = 'IER'
defaultLO = 0
LNBmodel = ' '
mode = ' '       #non-intialized value
default_com = '6'
##host = '192.168.1.96'
##host = '192.168.1.199'
host = '10.0.10.96'
gateway = '10.0.10.1'
mask = '255.255.255.0'


##===========================================================================    
def MSG_LOG(msg_string):
    if IS_LOG == 1:
        logfile.write(msg_string + "\r")
    print (msg_string )
    return

def timestamp (msg = ""):
    ts = time.asctime( time.localtime(time.time())) + " : " + msg
    print ts
    return ts

def pause_anykey():
    if SilentRunning != 1:
        demo = raw_input("paused - hit any key to continue")
    return

def setup_date():
    global RO_offset, DATE1, DATE2, DATE3, UTC
#capture date time parameters
    is_dst = time.daylight and time.localtime().tm_isdst > 0
    utc_offset = - (time.altzone if is_dst else time.timezone)
    RO_offset = str(utc_offset/36)
    UTC  = int(time.time())
    DATE1  =  time.strftime("%Y%m%d,%H%M%S", time.localtime(time.time()))
    DATE2  =  time.strftime("%Y%m%d-%H%M%S", time.localtime(time.time()))
    DATE3  =  time.strftime("%Y-%m-%d,%Hh%Mm", time.localtime(time.time()))
    return

def isWritable(path):
    import tempfile
    import errno
    try:
        testfile = tempfile.TemporaryFile(dir = path)
        testfile.close()
    except OSError as e:
        if e.errno == errno.EACCES:  # 13
            return False;
        e.filename = path
        raise
    return True;

def setup_logfile():
    global IS_LOG, mode
    global CID, UID, ODU_SN
    global logfilename, subdir_string
    global logfile
    if IS_LOG == 0:  
##----------- following from  function CIDUID()       
        UID = ""
        CID = ""
        if SilentRunning != 1:
            CID = raw_input("Enter 4 digit CID# / docket# (%s): " %(defaultCID))
            UID = raw_input("Enter 3 digit UID# (%s): " %(defaultUID))
        UID_LEN = len(UID)
        if CID == "":
            CID = defaultCID
        elif len(CID) < 4:
            CID = "000" + CID
        CID = CID[-4:]
        if UID == "":
            UID = str(101)
        elif UID_LEN == 1:
            UID = "00" + UID
        elif UID_LEN == 2:
            UID = "0" + UID
        elif UID_LEN > 3:
            UID = UID[-3:]
        ODU_SN = CID+ "-"+ UID
        print ('using ODU: '+ ODU_SN)
##-----------
        if os.access(os.getcwd(), os.W_OK) is True:
            print ("\nLocal DIR (%s) is writable" %(os.getcwd()))        
            IS_LOG = 1
            subdir_string = ""
##            subdir_string = CID + "-" + UID + "-ScriptData\\"		  # 2 level
##            if not os.path.exists(subdir_string):
##                os.makedirs(subdir_string)
##            print ("output subdirectory is: %s" %(subdir_string))    
            if UTC == 0:
                setup_date()
            logfilename = ODU_SN+ " "+ mode+ " RF TEST("+ DATE3 + ").txt"
            if os.path.isfile(logfilename):
                logfile = open(logfilename, 'a')
                print ("append to file: " + logfilename)
            else:
                logfile = open(logfilename, 'w')
                print ("created file: " + logfilename)
            print ("Test started at: %s\r\n" %(DATE3))
            logfile.write(APPtitle + " Ver:" + APPver + " Test started at"+ DATE3 +"\r\n")
            logfile.write("OPERATOR NAME: " +user_name + "\r\n")
        else:
            print ("\n\rLocal DIR (%s) is NOT writable\r\n" %(os.getcwd()))
            IS_LOG = 0
    elif IS_LOG== 2:
        logfile = open(logfilename, 'a')
        print ("append to file: " + logfilename)
##        print (os.path.isfile(logfilename))
##    pause_anykey()    
    return

def close_logfile(LFILE,option):
    global IS_LOG
    if IS_LOG==1:
        if option==0:
            LFILE.write("\r\nEND OF FILE\r\n")
            IS_LOG = 0
        else:
            IS_LOG = 2       
        LFILE.close()
    return
##===========================================================================                       
def setupFOX():
    global rm,N9918A
    
    defaultIP = "10.0.10.249"
    defaultFF = "MY53101679"
    altFoxSN = "MY53101680"
    FoxSN = raw_input("FieldFox N9918A SN:[%s]: " %(defaultFF))
    if FoxSN == "":
        FoxSN = defaultFF
       
    IPaddress = ""
    IPaddress = raw_input("Enter server IP address (local PC) xxx.xxx.xxx.xxx(%s): " %(defaultIP))
    if IPaddress == "":
        IPaddress = defaultIP
    print ("using N9918A sn: " + FoxSN + " IP: " + IPaddress)
    rm = visa.ResourceManager()
    N9918A = rm.open_resource('TCPIP0::' +  str(IPaddress) + '::inst0::INSTR')
##      N9918A = rm.open_resource('TCPIP0::10.0.10.146::inst0::INSTR')
##      N9918A = rm.open_resource('TCPIP0::10.0.10.130::inst0::INSTR')
    N9918A.timeout = 50000

    setup_logfile()
    logfile.write("-----------------------------------------")
    logfile.write("\n")
    logfile.write("RF Test Report for " + str(ODU_SN)+'\n')
    logfile.write("Measurements Done Using: FieldFox N9918A sn:" + str(FoxSN)+ " IP: " + IPaddress+'\n')
    logfile.write("Measurements Performed by: " + str(user_name)+'\n')
    logfile.write("Measurements Performed: "  + DATE3 +'\n\n')
    
    print("-----------------------------------------")
    print("\n")
    print("RF Test Report for " + str(ODU_SN)+'\n')
    print("Measurements Done Using: FieldFox N9918A sn:" + str(FoxSN)+'\n')
    print("Measurements Performed by: " + str(user_name)+'\n')
    print("Measurements Performed: "  + DATE3 +'\n\n')
                      
    return
##===========================================================================
def RUN_ODU11_KA2025():
   
    defaultPwrCal = -10
    PwrCal = raw_input("What is the Calibrated Power Level?[%s]" %(defaultPwrCal))
    if PwrCal == "":
        PwrCal = defaultPwrCal
    print (PwrCal)
    
    defaultComp = 30
    Comp = raw_input("What is the Compensation value?[%s]" %(defaultComp))
    if Comp == "":
        Comp = defaultComp
    print (Comp)
    

    defaultExpect = 60
    Expect = raw_input("What is the Expected Value?[%s]" %(defaultExpect))
    if Expect == "":
        Expect = defaultExpect
    print (Expect)
    
    InputLevel = 0
##--------------------------
    logfile.write("Instrument Calibrated at -10dBm output with a 20dB Attenuator"+'\n')
    logfile.write("Source Power is then set to -40dBm, therefore all RAW measurements require an offset compensation of 30dB."+'\n')
    logfile.write("This is done to prevent instrument saturation"+'\n')
    logfile.write("LNBs have an expected gain: " + str(Expect)+'\n')
    logfile.write("-----------------------------------------\n\n")
##    logfile.close()
    close_logfile(logfile,2)

    print("Instrument Calibrated at -10dBm output with a 20dB Attenuator"+'\n')
    print("Source Power is then set to -40dBm, therefore all RAW measurements require an offset compensation of 30dB."+'\n')
    print("This is done to prevent instrument saturation"+'\n')
    print("LNBs have an expected gain: " + str(Expect)+'\n')
    print("-----------------------------------------\n\n")
    
##--------------------------
    TestNum = "(Test 1.1)LNB-LNB1 Active Path Gain Vertical BAND1"
    isVSWR = 0
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KABAND2025.STA"')
    print ("---------------------\n" + TestNum)
    print ("Ensure 20dB attn in place and connect FF PORT1 to Online RF-IN and Power meter to IF-OUT (J3)")
    print (" SWITCH TO PATH A")
    print ("Remember to activate {RUN} on field FOX before running test below.")
    time.sleep(1)
    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##--------------------------
    TestNum = "(Test 1.2)LNB2-LNB2 Active Path Gain Vertical BAND1"
    isVSWR = 0
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KABAND2025.STA"')
    print ("---------------------\n" + TestNum)
    print ("Ensure 20dB attn in place and connect FF PORT1 to Online RF-IN and Power meter to IF-OUT (J3)")
    print (" SWITCH TO PATH B")
    print ("Remember to activate {RUN} on field FOX before running test below.")
    time.sleep(1)
    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##--------------------------
## Return Loss
        
    TestNum = "(Test 1.61) Input Return Loss at ON-line RF-IN Vertical"
    isVSWR = 1
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KABAND2025VSWR.STA"')
    print ("---------------------\n" + TestNum)
    print ("Ensure 20dB attn is removed and connect to FF PORT1 to to Online RF-IN")
    print (" SWITCH TO PATH A")
    print ("Remember to activate {RUN} on field FOX before running test below.")
    time.sleep(1)
    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##--------------------------
    TestNum = "(Test 1.60)Output Return Loss at IF-OUT Port (J3)"
    isVSWR = 1
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KABAND2025VSWROUT.STA"')
    print ("---------------------\n" + TestNum)
    print ("Ensure 20dB attn is removed and connect to FF PORT1 to IF-OUT (J3)")
    print (" SWITCH TO PATH A")
    print ("Remember to activate {RUN} on field FOX before running test below.")
    time.sleep(1)
    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##--------------------------

    N9918A.close()
    rm.close()
    setup_logfile()
    close_logfile(logfile,0)
    return
##===========================================================================
##def run_1for2_tests():
##    #(Test 2.1) LNB-LNB1 Active Path Gain (High band) BAND1
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print ('(Test 2.1)LNB-LNB1 Active Path Gain (High band) BAND1, press Enter')
##
##    TestNum = "(Test 2.1)LNB-LNB1 Active Path Gain Vertical BAND1"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##
##    #(Test 2.2) LNB-LNB1 Active Coupled Output
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test 2.2)LNB-LNB1 Output BAND1 Coupled Output, press Enter')
##
##    TestNum = "(Test 2.2)LNB-LNB1 Vertical Output BAND1 Coupled Output"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##    
##    #(Test 2.3)LNB1-LNB1 Active Path using Cross Guide Coupler Input
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test 2.3)LNB1-LNB1 Active Path using Cross Guide Coupler Input, press Enter')
##    time.sleep(DELAY)
##    TestNum = "(Test 2.3)LNB1-LNB1 Vertical Active Path using Cross Guide Coupler Input"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM30C(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##
##    #(Test 2.4) LNB2-LNB2 Active Path Gain (High band) BAND2
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test 2.4)LNB2-LNB2 Active Path Gain (High band) BAND2, press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test 2.4)LNB2-LNB2 Horizontal Active Path Gain (High band) BAND2"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##
##    #(Test 2.4) LNB-LNB2 Active Coupled Output
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test 2.4)LNB-LNB2 Output BAND1 Coupled Output, press Enter')
##
##    TestNum = "(Test 2.4)LNB-LNB2 Horizontal Output BAND1 Coupled Output"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##        
##
##    
##
##    #(Test 2.6)LNB1-LNB1 Standby TEST Path
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
##    print("Remember to activate {RUN} on field FOX before running test below.")
##    print ('(Test 2.6)LNB1-LNB1 Standby TEST Path, press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test 2.6)LNB1-LNB1 Standby TEST Path"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)    
##
##
##    #Test 2.7 (Test 2.7) LNB2-LNB2 Active Path using Cross Guide Coupler Input
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test 2.7) LNB2-LNB2 Active Path using Cross Guide Coupler Input, press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test 2.7) LNB2-LNB2 Horizontal Active Path using Cross Guide Coupler Input"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM30C(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##
##    #(Test 2.8) LNB3-LNB3 Active Path Gain (LNB1 offline)
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test 2.8) LNB3-LNB3 Active Path Gain (LNB1 offline), press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test 2.8) LNB3-LNB3 Vertical Active Path Gain (LNB1 offline)"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##    #(Test 2.9) LNB3-LNB3 Active Path Gain (LNB2 offline)
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test 2.9) LNB3-LNB3 Active Path Gain (LNB2 offline), press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test 2.9) LNB3-LNB3 Horizontal5 Active Path Gain (LNB2 offline)"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)        
##        
#### Return Loss        
##
##    #(Test 2.10.1) Input Return Loss at ON-line RHCP RF-IN Vertical
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWR.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test 2.10.1) Input Return Loss at ON-line RHCP RF-INVertical, press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test 2.10.1) Input Return Loss at ON-line RHCP RF-INVertical"
##    isVSWR = 1
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##    #(Test 2.10.2)Input Return Loss at ON-line LHCP RF-INHorizontal
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWR.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test 2.10.2)Input Return Loss at ON-line LHCP RF-INHorizontal, press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test 2.10.2)Input Return Loss at ON-line LHCP RF-IN Horizontal"
##    isVSWR = 1
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##    #(Test 2.10.3) Input Return Loss at OFF-line RF-IN Port
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWR.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test 2.10.3) Input Return Loss at OFF-line RF-IN Port, press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test 2.10.3) Input Return Loss at OFF-line RF-IN Port"
##    isVSWR = 1
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##
##    #(Test2.10.4)Input Return Loss at RHCP Test RF-INVertical
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWR.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test2.10.4)Input Return Loss at RHCP Test RF-IN Vertical, press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test2.10.4)Input Return Loss at RHCP Test RF-IN Vertical"
##    isVSWR = 1
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##    #(Test2.10.5)Input Return Loss at LHCP Test RF-INHorizontal
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWR.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test2.10.5)Input Return Loss at LHCP Test RF-IN Horizontal, press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test2.10.5)Input Return Loss at LHCP Test RF-IN Horizontal"
##    isVSWR = 1
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##
##    #(Test2.10.7)Output Return Loss at IF-OUT Port (J4)
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWROUT.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test2.10.7)Output Return Loss at IF-OUT Port (J4), press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test2.10.7)Output Return Loss at IF-OUT Port (J4)"
##    isVSWR = 1
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##    #(Test2.10.9)Output Return Loss at IF-OUT Port (J3)
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWROUT.STA"')
##    print ("Remember to activate {RUN} on field FOX before running test below.")
##    print('(Test2.10.9)Output Return Loss at IF-OUT Port (J3), press Enter')
##
##    time.sleep(DELAY)
##    TestNum = "(Test2.10.9)Output Return Loss at IF-OUT Port (J3)"
##    isVSWR = 1
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##
##    
##    N9918A.close()
##    rm.close()
##    return
##===========================================================================

def activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect):

    ok= False

    while (ok == False):
        pictureFileName = ODU_SN + " " + TestNum
        #activating marker 1
        N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
##        #setting marker 1 to max
        N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
##        #fetching marker 1 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        test1 = float(maximum[0]) + 31.8
        SWRV = float(maximum[0])
        



        #activating marker 2
        N9918A.write(':CALCulate:SELected:MARKer2:ACTivate')
##        #setting marker 2 to max
        N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
##        #fetching marker 2 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        test2 = minimum[0] + 31.8
        y = (test1 - test2)/2
        
        
        if (isVSWR == 0):
            Mid = test2 + y
            Tru = float(Comp) + Mid
            print "minimum: %f " %test2
            print "maximum: %f" %test1
            print "Raw: %f "  %Mid
            print "Raw + Compensation: " + str(Tru)
            print "Expected: " + str(Expect)
            
##            if (y > 4) or (minimum < -4):
##                print "Failed"
##            else:
##                print "Passed"

        else:
            median = test1
            print "Maximum: %f" %SWRV
##            if (maximum > -10):
##                print "Failed"
##            else:
##                print "Passed"
                
##        )

        reply = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            ok = True
            print "Yes"
        if reply[0] == 'n':
            ok = False
            print "NO"
    

    #setting the save directory to internal memory
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    
    

    #setting the format to save a screenshot
    N9918A.values_format.is_binary = True
    N9918A.values_format.datatype = 'B'
    N9918A.values_format.is_big_endian = False
    N9918A.values_format.container = bytearray

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")

    #creating a file with the product number to write the image data to it
    save_dir = open(pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

    #output data to Text File
    setup_logfile()
##    logfile = open(logfilename+".txt","a")
    logfile.write(pictureFileName)
    logfile.write("\n")
    logfile.write("ScreenShot FileName: " + pictureFileName + ".png")
    logfile.write("\n")
    
    if (isVSWR == 0):
        CsvFileName = pictureFileName + ".csv"
        logfile.write("RAW Gain: %s " %Mid)
        logfile.write("\n")
        logfile.write("Expected: %s " %Expect)
        logfile.write("\n")
        logfile.write("RAW + Compensation: %s" %Tru)
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
    else:
        CsvFileName = pictureFileName + "CT.csv"
        SPFileName = pictureFileName + ".s2p"
        logfile.write("Maximum (worst) Input Return Loss(VSWR): %s " %SWRV )
        logfile.write("\n")
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
        N9918A.write(":MMEMory:STORe:SNP '%s'" %SPFileName)

    #logfile.write("-----------------------------------")
    logfile.write("\n")
    logfile.write("\n")
    close_logfile(logfile,2)
##    logfile.close

##===========================================================================
def activateMaxMinM20C(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect):

    ok= False

    while (ok == False):
        pictureFileName = ODU_SN + " " + TestNum
        #activating marker 1
##        N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
##        #setting marker 1 to max
        N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
##        #fetching marker 1 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        test1 = float(maximum[0]) + 31.8
        SWRV = float(maximum[0])
        



        #activating marker 2
##        N9918A.write(':CALCulate:SELected:MARKer2:ACTivate')
##        #setting marker 2 to max
        N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
##        #fetching marker 2 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        test2 = minimum[0] + 31.8
        y = (test1 - test2)/2
        
        
        if (isVSWR == 0):
            Mid = test2 + y
            Tru = float(Comp) + Mid
            print "minimum: %f " %test2
            print "maximum: %f" %test1
            print "Raw: %f "  %Mid
            print "Raw + Compensation: " + str(Tru)
            print "Expected: " + str((int(Expect) - int(20)))
            
##            if (y > 4) or (minimum < -4):
##                print "Failed"
##            else:
##                print "Passed"

        else:
            median = test1
            print "Maximum: %f" %SWRV
##            if (maximum > -10):
##                print "Failed"
##            else:
##                print "Passed"
                
##        )

        reply = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            ok = True
            print "Yes"
        if reply[0] == 'n':
            ok = False
            print "NO"
    

    #setting the save directory to internal memory
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))

    #setting the format to save a screenshot
    N9918A.values_format.is_binary = True
    N9918A.values_format.datatype = 'B'
    N9918A.values_format.is_big_endian = False
    N9918A.values_format.container = bytearray

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")

    #creating a file with the product number to write the image data to it
    save_dir = open(pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

    #output data to Text File
    setup_logfile()
##    logfile = open(logfilename+".txt","a")
    logfile.write(pictureFileName)
    logfile.write("\n")
    logfile.write("ScreenShot FileName: " + pictureFileName + ".png")
    logfile.write("\n")
    if (isVSWR == 0):
        CsvFileName = pictureFileName + ".csv"
        logfile.write("RAW Gain: %s " %Mid)
        logfile.write("\n")
        logfile.write("Expected: %s " %Expect)
        logfile.write("\n")
        logfile.write("RAW + Compensation: %s" %Tru)
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
    else:
        logfile.write("Maximum (worst) Input Return Loss(VSWR): %s " %SWRV )
        logfile.write("\n")

    #logfile.write("-----------------------------------")
    logfile.write("\n")
    logfile.write("\n")
    close_logfile(logfile,2)
##    logfile.close   
##===========================================================================
def activateMaxMinM30C(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect):

    ok= False

    while (ok == False):
        pictureFileName = ODU_SN + " " + TestNum
        #activating marker 1
##        N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
##        #setting marker 1 to max
        N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
##        #fetching marker 1 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        test1 = float(maximum[0]) + 31.8
        SWRV = float(maximum[0])
        



        #activating marker 2
##        N9918A.write(':CALCulate:SELected:MARKer2:ACTivate')
##        #setting marker 2 to max
        N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
##        #fetching marker 2 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        test2 = minimum[0] + 31.8
        y = (test1 - test2)/2
        
        
        if (isVSWR == 0):
            Mid = test2 + y
            Tru = Mid
            print "minimum: %f " %test2
            print "maximum: %f" %test1
            print "Raw: %f "  %Mid
            print "Raw + Compensation: " + str(Tru)
            print "Expected: " + str(float(Expect)-30)
            
##            if (y > 4) or (minimum < -4):
##                print "Failed"
##            else:
##                print "Passed"

        else:
            median = test1
            print "Maximum: %f" %SWRV
##            if (maximum > -10):
##                print "Failed"
##            else:
##                print "Passed"
                
##        )

        reply = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            ok = True
            print "Yes"
        if reply[0] == 'n':
            ok = False
            print "NO"
    

    #setting the save directory to internal memory
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))

    #setting the format to save a screenshot
    N9918A.values_format.is_binary = True
    N9918A.values_format.datatype = 'B'
    N9918A.values_format.is_big_endian = False
    N9918A.values_format.container = bytearray

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")

    #creating a file with the product number to write the image data to it
    save_dir = open(pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

    #output data to Text File
    setup_logfile()
##    logfile = open(logfilename+".txt","a")
    logfile.write(pictureFileName)
    logfile.write("\n")
    logfile.write("ScreenShot FileName: " + pictureFileName + ".png")
    logfile.write("\n")
    if (isVSWR == 0):
        CsvFileName = pictureFileName + ".csv"
        logfile.write("RAW Gain: %s " %Mid)
        logfile.write("\n")
        logfile.write("Expected: %s " %Expect)
        logfile.write("\n")
        logfile.write("RAW + Compensation: %s" %Tru)
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
    else:
        logfile.write("Maximum (worst) Input Return Loss(VSWR): %s " %SWRV )
        logfile.write("\n")

    #logfile.write("-----------------------------------")
    logfile.write("\n")
    logfile.write("\n")
    close_logfile(logfile,2)
##    logfile.close
##===========================================================================
##---------------------------------------------------------------------------------------
## END of INITIALIZATIONS
##---------------------------------------------------------------------------------------
## MAIN CODE follows:
IS_H = 0
IS_notDONE = 1
APPtitle="ODU RF TEST"
APPver = str('(' + os.path.basename(__file__)) + ') Modified: ' + str(datetime.fromtimestamp(int(os.path.getmtime(__file__))))
print ("%s,%s" %(APPtitle, APPver))

user_name = "code test"
if SilentRunning != 1: user_name = raw_input("Enter Operator Name: ")
while (IS_notDONE):
    if IS_H == 1 :

        print ("\r")
        print ("Ka       - run Ka 1:1 IER LO=20.25GHz\r")                  
        print ("LS       - List Log\r")
        print ("H        - Toggle menu option\r")
        print ("Q        - QUIT\r")


    user_input = raw_input(">>>>> Enter mode,type H for help: (default is Q): ")
    mode = user_input.upper()
    if mode == "":
        mode = "Q"
    setup_date()
    print ("Using %s mode\r\n" %(mode))
#******************************************
    if mode == "Q":
        break
#******************************************
    elif mode == "H":
        if IS_H == 1:
             IS_H = 0
        else:
             IS_H = 1
#******************************************
    elif mode == "KA":
        mode="ODU11-Ka-IER-LO2025"
        IS_notDONE = 0
        setupFOX()
        RUN_ODU11_KA2025()               
#******************************************                      
    elif mode == "LS":
        import serial.tools.list_ports
        ports = serial.tools.list_ports.comports()
        for port, desc, hwid in sorted(ports):
            print("{}: {} [{}]".format(port, desc, hwid))

        if IS_LOG == 1:
            print ("logfile: %s" %(logfile.name))
        else:
             print ("Log file closed.\r\n")
        if IS_SER  == 1:
             print ("Using serial port COM%s\r\n" %(com_port))
        else:
             print ("Serial port closed.\r\n")
        pause_anykey()
#******************************************
    else:
        print ("mode not defined\n")
        break

#****************
if IS_LOG != 0:
    close_logfile(logfile,2)
##    logfile.close()
if IS_SER != 0:
    ser.close()
#================================================================================






