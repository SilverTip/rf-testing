import visa
import time
import datetime


def take_screenshot():
    ODU_SN = raw_input('Enter ODU serial Number:')
    #T_M_Equip = raw_input('FieldFox SN:')
    T_M_Equip = "FieldFox N9918A MY53101680"
    T_Runner = raw_input('Enter your name:')
    PwrCal = raw_input('What is the Calibrated Power Level?')
    Comp = raw_input('What is the Compensation value?')
    Expect = raw_input('What is the Expected Value?')
    InputLevel = 0
    fileName = ODU_SN + " RF Test"
    timeNow = datetime.datetime.now()
    
    DELAY = 1
    LONG_DELAY = 30
  
    rm = visa.ResourceManager()
    N9918A = rm.open_resource('TCPIP0::10.0.10.105::inst0::INSTR')
    N9918A.timeout = 50000

    #openfile = "file"
    total = 0.5
    totalstr = str(total)
    file = open("%s.txt" % (fileName), "a")
    #file.write(totalstr)
    file = open(fileName+".txt","a")
    file.write("-----------------------------------------")
    file.write("\n")
    file.write("RF Test Report for " + str(ODU_SN))
    file.write("\n")
    file.write("Measurements Done Using: " + str(T_M_Equip))
    file.write("\n")
    file.write("Measurements Performed by: " + str(T_Runner))
    file.write("\n")
    file.write("Measurements Performed: "  + str(timeNow))
    file.write("\n")
    file.write("\n")
    file.write("Instrument Calibrated at -10dBm output with a 20dB Attenuator")
    file.write("\n")
    file.write("Source Power is then set to -40dBm, therefore all RAW measurements require an offset compensation of 30dB.")
    file.write("\n")
    file.write("This is done to prevent instrument saturation")
    file.write("\n")
    file.write("LNBs have an expected gain of 60dBm.")
    file.write("\n")
    file.write("-----------------------------------------")
    file.write("\n")
    file.write("\n")
    file.close()

    #Test 1.1 LNB/LNA1 Active Path Gain
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND106.STA"')
    prompt_user = raw_input('T1.1  LNB1 Active Path Gain, press Enter')
##    prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 1.1) LNB1 Active Path Gain"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)
        

    #Test 1.2 LNB2 Active Path Gain
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    #N9918A.write(':MMEMory:LOAD:STATe "NORSAT BAND1 KA.STA"')
    prompt_user = raw_input('LNB2 Active Path Gain, press Enter')
##    prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 1.2) LNB2 Active Path Gain"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #Test 1.3 LNB/LNA1 Active Path using Coax Dir Coupler output (requires 20CC option)
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    #N9918A.write(':MMEMory:LOAD:STATe "NORSAT BAND1 KA.STA"')
    prompt_user = raw_input('(Test 1.3)Active Path using Coax Dir Coupler output')
##    prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 1.3)Active Path using Coax Dir Coupler output"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)
        

    #Test 1.5 LNB1 Standby TEST Path 
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND106.STA"')
    prompt_user = raw_input('T1.5 LNB1 Standby TEST Path , press Enter')
##    prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 1.5) LNB1 Standby TEST Path "
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)
		
    #T1.6 LNB1 Cross Guide Coupler Input
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND106.STA"')
    prompt_user = raw_input('T1.6 LNB1 Cross Guide Coupler Input , press Enter')
##    prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 1.5) LNB1 Cross Guide Coupler Input "
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


##    #Test 1.4 Verify LNB2 Active path to Aux1
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND106.STA"')
##    prompt_user = raw_input('T1.4 Activate LNB path 1 & Connect power sensor to AUX1 and fieldfox & Connect field fox port 1 to RF input and press Enter')
##    prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
##    time.sleep(DELAY)
##    TestNum = "(Test 1.4) RFin to AUX1 -- LNB2 Active"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)
##
##
##    #Test 1.5 Verify Offlne path to Aux1
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND106.STA"')
##    prompt_user = raw_input('T1.5 Activate LNB path 1 & Connect power sensor to AUX1 and fieldfox & Connect field fox port 1 to Offlne input and press Enter')
##    prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
##    time.sleep(DELAY)
##    TestNum = "(Test 1.5) Offlne to AUX1 -- LNB1 Active"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

   

    #Test 1.6.1 Input Return Loss at ON-line RF-IN
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KABANDVSWR1925.STA"')
    prompt_user = raw_input('T1.6.1 Input Return Loss at ON-line RF-IN, Press Enter')
##    prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 1.6.1) Input Return Loss at ON-line RF-IN"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #Test 1.6.2 Input Return Loss at OFF-line RF-IN Port
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KABANDVSWRR.STA"')
    prompt_user = raw_input('1.6.2 Input Return Loss at OFF-line RF-IN Port, Press Enter')
##    prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 1.6.2) Input Return Loss at OFF-line RF-IN Port"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

##    #Test 1.5 Verify LNB1 Active path AUX 4 to AUX 1
##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "NORSAT BAND GAIN.STA"')
##    prompt_user = raw_input('T1.5 Activate LNB path 1 & Connect power sensor to AUX1 & Connect fieldfox port 1 to AUX4 and press Enter')
##    prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
##    time.sleep(DELAY)
##    TestNum = "(Test 1.5) AUX4 to AUX1 -- LNB1 Active"
##    isVSWR = 0
##
##    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
##    if testYes[0] == 'y':
##        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #Test 1.6.2 Input Return Loss @ AUX3 N/A
    #N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    #N9918A.write(':MMEMory:LOAD:STATe "6716 KA BAND1 VSWR.STA"')
    #prompt_user = raw_input('connect Field Fox Port 1 to AUX4')
    #prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
    #time.sleep(DELAY)
    #TestNum = "(Test 1.6.2) Input Return Loss at AUX4"
    #isVSWR = 1

    #testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    #if testYes[0] == 'y':
        #activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #Test 1.6.3 Input Return Loss @ AUX4
    #N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    #N9918A.write(':MMEMory:LOAD:STATe "6716 KA BAND1 VSWR.STA"')
    #prompt_user = raw_input('Remove 20dB pad on FieldFox andconnect to RF in')
    #prompt_user = raw_input('Check ULC is on Band 1 and press Enter')
    #time.sleep(DELAY)
    #TestNum = "(Test 1.6.1) Input Return Loss at AUX4"
    #isVSWR = true

    #testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    #if testYes[0] == 'y':
        #activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)
    



##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "6716 KA BAND2.STA')
##    prompt_user = raw_input('Check ULC is on Band 2 and press Enter')
##    time.sleep(20)




##    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
##    N9918A.write(':MMEMory:LOAD:STATe "6716 KA BAND3.STA')
##    prompt_user = raw_input('Check ULC is on Band 3 and press Enter')
##    time.sleep(20)
    
    N9918A.close()
    rm.close()


def activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect):

    ok= False

    while (ok == False):
        pictureFileName = ODU_SN + " " + TestNum
        #activating marker 1
        N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
##        #setting marker 1 to max
        N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
##        #fetching marker 1 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        test1 = float(maximum[0]) + 30.8
        SWRV = float(maximum[0])
        



        #activating marker 2
        N9918A.write(':CALCulate:SELected:MARKer2:ACTivate')
##        #setting marker 2 to max
        N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
##        #fetching marker 2 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        test2 = minimum[0] + 30.8
        y = (test1 - test2)/2
        
        
        if (isVSWR == 0):
            Mid = test2 + y
            Tru = float(Comp) + Mid
            print "minimum: %f " %test2
            print "maximum: %f" %test1
            print "Raw: %f "  %Mid
            print "Raw + Compensation: " + str(Tru)
            print "Expected: " + str(Expect)
            
##            if (y > 4) or (minimum < -4):
##                print "Failed"
##            else:
##                print "Passed"

        else:
            median = test1
            print "Maximum: %f" %SWRV
##            if (maximum > -10):
##                print "Failed"
##            else:
##                print "Passed"
                
##        )

        reply = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            ok = True
            print "Yes"
        if reply[0] == 'n':
            ok = False
            print "NO"
    

    #setting the save directory to internal memory
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    
    

    #setting the format to save a screenshot
    N9918A.values_format.is_binary = True
    N9918A.values_format.datatype = 'B'
    N9918A.values_format.is_big_endian = False
    N9918A.values_format.container = bytearray

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")

    #creating a file with the product number to write the image data to it
    save_dir = open(pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

    #output data to Text File
    outputFile = open(fileName+".txt","a")
    outputFile.write(pictureFileName)
    outputFile.write("\n")
    outputFile.write("ScreenShot FileName: " + pictureFileName + ".png")
    outputFile.write("\n")
    
    if (isVSWR == 0):
        CsvFileName = pictureFileName + ".csv"
        outputFile.write("RAW Gain: %s " %Mid)
        outputFile.write("\n")
        outputFile.write("Expected: %s " %Expect)
        outputFile.write("\n")
        outputFile.write("RAW + Compensation: %s" %Tru)
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
    else:
        CsvFileName = pictureFileName + "CT.csv"
        SPFileName = pictureFileName + ".s2p"
        outputFile.write("Maximum (worst) Input Return Loss(VSWR): %s " %SWRV )
        outputFile.write("\n")
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
        N9918A.write(":MMEMory:STORe:SNP '%s'" %SPFileName)

    #outputFile.write("-----------------------------------")
    outputFile.write("\n")
    outputFile.write("\n")
    
    outputFile.close

   


    

take_screenshot()


