import pyvisa as visa
import time
import datetime


def take_screenshot():
    ODU_SN = raw_input('Enter ODU serial Number:')
    #T_M_Equip = raw_input('FieldFox SN:')
    T_M_Equip = "FieldFox N9918A MY53101679"
    T_Runner = raw_input('Enter your name:')
    PwrCal = raw_input('What is the Calibrated Power Level?')
    Comp = raw_input('What is the Compensation value?')
    Expect = raw_input('What is the Expected Value?')
    InputLevel = 0
    fileName = ODU_SN + " RF Test"
    timeNow = datetime.datetime.now()
    
    DELAY = 1
    LONG_DELAY = 30
    defaultIP = "10.0.10.249"

    IPaddress = ""
    IPaddress = raw_input("Enter server IP address (local PC) xxx.xxx.xxx.xxx(%s): " %(defaultIP))
    if IPaddress == "":
        IPaddress = defaultIP
    print ("IPaddress = %s" %(IPaddress))

    rm = visa.ResourceManager()

    N9918A = rm.open_resource('TCPIP0::' +  str(IPaddress) + '::inst0::INSTR')
##    N9918A = rm.open_resource('TCPIP0::10.0.10.146::inst0::INSTR')
##   N9918A = rm.open_resource('TCPIP0::10.0.10.130::inst0::INSTR')
    N9918A.timeout = 50000 

    #openfile = "file"
    total = 0.5
    totalstr = str(total)
    file = open("%s.txt" % (fileName), "a")
    #file.write(totalstr)
    file = open(fileName+".txt","a")
    file.write("-----------------------------------------")
    file.write("\n")
    file.write("RF Test Report for " + str(ODU_SN))
    file.write("\n")
    file.write("Measurements Done Using: " + str(T_M_Equip))
    file.write("\n")
    file.write("Measurements Performed by: " + str(T_Runner))
    file.write("\n")
    file.write("Measurements Performed: "  + str(timeNow))
    file.write("\n")
    file.write("\n")
    file.write("Instrument Calibrated at -10dBm output with a 20dB Attenuator")
    file.write("\n")
    file.write("This is done to prevent instrument saturation")
    file.write("\n")
    file.write("LNBs have an expected gain: " + str(Expect))
    file.write("\n")
    file.write("-----------------------------------------")
    file.write("\n")
    file.write("\n")
    file.close()

    #(Test 2.1) LNB-LNB1 Active Path Gain (High band) BAND1
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
    print('(Test 2.1)LNB-LNB1 Active Path Gain (High band) BAND1, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    #time.sleep(DELAY)
    TestNum = "(Test 2.1)LNB-LNB1 Active Path Gain Vertical BAND1"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test 2.2) LNB-LNB1 Active Coupled Output
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
    print('(Test 2.2)LNB-LNB1 Output BAND1 Coupled Output, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    #time.sleep(DELAY)
    TestNum = "(Test 2.2)LNB-LNB1 Vertical Output BAND1 Coupled Output"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

      


    #(Test 2.3)LNB1-LNB1 Active Path using Cross Guide Coupler Input
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
    print('(Test 2.3)LNB1-LNB1 Active Path using Cross Guide Coupler Input, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.3)LNB1-LNB1 Vertical Active Path using Cross Guide Coupler Input"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM30C(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test 2.4) LNB2-LNB2 Active Path Gain (High band) BAND2
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
    print('(Test 2.4)LNB2-LNB2 Active Path Gain (High band) BAND2, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.4)LNB2-LNB2 Horizontal Active Path Gain (High band) BAND2"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test 2.4) LNB-LNB2 Active Coupled Output
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
    print('(Test 2.4)LNB-LNB2 Output BAND1 Coupled Output, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    #time.sleep(DELAY)
    TestNum = "(Test 2.4)LNB-LNB2 Horizontal Output BAND1 Coupled Output"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)
        

    

    #(Test 2.6)LNB1-LNB1 Standby TEST Path
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
    print('(Test 2.6)LNB1-LNB1 Standby TEST Path, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.6)LNB1-LNB1 Standby TEST Path"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)    


    #Test 2.7 (Test 2.7) LNB2-LNB2 Active Path using Cross Guide Coupler Input
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
    print('(Test 2.7) LNB2-LNB2 Active Path using Cross Guide Coupler Input, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.7) LNB2-LNB2 Horizontal Active Path using Cross Guide Coupler Input"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM30C(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test 2.8) LNB3-LNB3 Active Path Gain (LNB1 offline)
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
    print('(Test 2.8) LNB3-LNB3 Active Path Gain (LNB1 offline), press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.8) LNB3-LNB3 Vertical Active Path Gain (LNB1 offline)"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #(Test 2.9) LNB3-LNB3 Active Path Gain (LNB2 offline)
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBAND.STA"')
    print('(Test 2.9) LNB3-LNB3 Active Path Gain (LNB2 offline), press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.9) LNB3-LNB3 Horizontal5 Active Path Gain (LNB2 offline)"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)        
        
## Return Loss        

    #(Test 2.10.1) Input Return Loss at ON-line RHCP RF-IN Vertical
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWR.STA"')
    print('(Test 2.10.1) Input Return Loss at ON-line RHCP RF-INVertical, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.10.1) Input Return Loss at ON-line RHCP RF-INVertical"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #(Test 2.10.2)Input Return Loss at ON-line LHCP RF-INHorizontal
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWR.STA"')
    print('(Test 2.10.2)Input Return Loss at ON-line LHCP RF-INHorizontal, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.10.2)Input Return Loss at ON-line LHCP RF-IN Horizontal"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #(Test 2.10.3) Input Return Loss at OFF-line RF-IN Port
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWR.STA"')
    print('(Test 2.10.3) Input Return Loss at OFF-line RF-IN Port, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.10.3) Input Return Loss at OFF-line RF-IN Port"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test2.10.4)Input Return Loss at RHCP Test RF-INVertical
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWR.STA"')
    print('(Test2.10.4)Input Return Loss at RHCP Test RF-IN Vertical, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test2.10.4)Input Return Loss at RHCP Test RF-IN Vertical"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #(Test2.10.5)Input Return Loss at LHCP Test RF-INHorizontal
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWR.STA"')
    print('(Test2.10.5)Input Return Loss at LHCP Test RF-IN Horizontal, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test2.10.5)Input Return Loss at LHCP Test RF-IN Horizontal"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test2.10.7)Output Return Loss at IF-OUT Port (J4)
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWROUT.STA"')
    print('(Test2.10.7)Output Return Loss at IF-OUT Port (J4), press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test2.10.7)Output Return Loss at IF-OUT Port (J4)"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #(Test2.10.9)Output Return Loss at IF-OUT Port (J3)
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-KUBANDVSWROUT.STA"')
    print('(Test2.10.9)Output Return Loss at IF-OUT Port (J3), press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test2.10.9)Output Return Loss at IF-OUT Port (J3)"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    
    N9918A.close()
    rm.close()


def activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect):

    ok= False

    while (ok == False):
        pictureFileName = ODU_SN + " " + TestNum
        #activating marker 1
        N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
##        #setting marker 1 to max
        N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
##        #fetching marker 1 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        test1 = float(maximum[0]) + 31.8
        SWRV = float(maximum[0])
        



        #activating marker 2
        N9918A.write(':CALCulate:SELected:MARKer2:ACTivate')
##        #setting marker 2 to max
        N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
##        #fetching marker 2 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        test2 = minimum[0] + 31.8
        y = (test1 - test2)/2
        
        
        if (isVSWR == 0):
            Mid = test2 + y
            Tru = float(Comp) + Mid
            print "minimum: %f " %test2
            print "maximum: %f" %test1
            print "Raw: %f "  %Mid
            print "Raw + Compensation: " + str(Tru)
            print "Expected: " + str(Expect)
            
##            if (y > 4) or (minimum < -4):
##                print "Failed"
##            else:
##                print "Passed"

        else:
            median = test1
            print "Maximum: %f" %SWRV
##            if (maximum > -10):
##                print "Failed"
##            else:
##                print "Passed"
                
##        )

        reply = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            ok = True
            print "Yes"
        if reply[0] == 'n':
            ok = False
            print "NO"
    

    #setting the save directory to internal memory
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    
    

    #setting the format to save a screenshot
    N9918A.values_format.is_binary = True
    N9918A.values_format.datatype = 'B'
    N9918A.values_format.is_big_endian = False
    N9918A.values_format.container = bytearray

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")

    #creating a file with the product number to write the image data to it
    save_dir = open(pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

    #output data to Text File
    outputFile = open(fileName+".txt","a")
    outputFile.write(pictureFileName)
    outputFile.write("\n")
    outputFile.write("ScreenShot FileName: " + pictureFileName + ".png")
    outputFile.write("\n")
    
    if (isVSWR == 0):
        CsvFileName = pictureFileName + ".csv"
        outputFile.write("RAW Gain: %s " %Mid)
        outputFile.write("\n")
        outputFile.write("Expected: %s " %Expect)
        outputFile.write("\n")
        outputFile.write("RAW + Compensation: %s" %Tru)
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
    else:
        CsvFileName = pictureFileName + "CT.csv"
        SPFileName = pictureFileName + ".s2p"
        outputFile.write("Maximum (worst) Input Return Loss(VSWR): %s " %SWRV )
        outputFile.write("\n")
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
        N9918A.write(":MMEMory:STORe:SNP '%s'" %SPFileName)

    #outputFile.write("-----------------------------------")
    outputFile.write("\n")
    outputFile.write("\n")
    
    outputFile.close


def activateMaxMinM20C(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect):

    ok= False

    while (ok == False):
        pictureFileName = ODU_SN + " " + TestNum
        #activating marker 1
##        N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
##        #setting marker 1 to max
        N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
##        #fetching marker 1 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        test1 = float(maximum[0]) + 31.8
        SWRV = float(maximum[0])
        



        #activating marker 2
##        N9918A.write(':CALCulate:SELected:MARKer2:ACTivate')
##        #setting marker 2 to max
        N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
##        #fetching marker 2 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        test2 = minimum[0] + 31.8
        y = (test1 - test2)/2
        
        
        if (isVSWR == 0):
            Mid = test2 + y
            Tru = float(Comp) + Mid
            print "minimum: %f " %test2
            print "maximum: %f" %test1
            print "Raw: %f "  %Mid
            print "Raw + Compensation: " + str(Tru)
            print "Expected: " + str((int(Expect) - int(20)))
            
##            if (y > 4) or (minimum < -4):
##                print "Failed"
##            else:
##                print "Passed"

        else:
            median = test1
            print "Maximum: %f" %SWRV
##            if (maximum > -10):
##                print "Failed"
##            else:
##                print "Passed"
                
##        )

        reply = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            ok = True
            print "Yes"
        if reply[0] == 'n':
            ok = False
            print "NO"
    

    #setting the save directory to internal memory
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))

    #setting the format to save a screenshot
    N9918A.values_format.is_binary = True
    N9918A.values_format.datatype = 'B'
    N9918A.values_format.is_big_endian = False
    N9918A.values_format.container = bytearray

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")

    #creating a file with the product number to write the image data to it
    save_dir = open(pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

    #output data to Text File
    outputFile = open(fileName+".txt","a")
    outputFile.write(pictureFileName)
    outputFile.write("\n")
    outputFile.write("ScreenShot FileName: " + pictureFileName + ".png")
    outputFile.write("\n")
    if (isVSWR == 0):
        CsvFileName = pictureFileName + ".csv"
        outputFile.write("RAW Gain: %s " %Mid)
        outputFile.write("\n")
        outputFile.write("Expected: %s " %Expect)
        outputFile.write("\n")
        outputFile.write("RAW + Compensation: %s" %Tru)
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
    else:
        outputFile.write("Maximum (worst) Input Return Loss(VSWR): %s " %SWRV )
        outputFile.write("\n")

    #outputFile.write("-----------------------------------")
    outputFile.write("\n")
    outputFile.write("\n")
    
    outputFile.close   

def activateMaxMinM30C(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect):

    ok= False

    while (ok == False):
        pictureFileName = ODU_SN + " " + TestNum
        #activating marker 1
##        N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
##        #setting marker 1 to max
        N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
##        #fetching marker 1 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        test1 = float(maximum[0]) + 31.8
        SWRV = float(maximum[0])
        



        #activating marker 2
##        N9918A.write(':CALCulate:SELected:MARKer2:ACTivate')
##        #setting marker 2 to max
        N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
##        #fetching marker 2 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        test2 = minimum[0] + 31.8
        y = (test1 - test2)/2
        
        
        if (isVSWR == 0):
            Mid = test2 + y
            Tru = Mid
            print "minimum: %f " %test2
            print "maximum: %f" %test1
            print "Raw: %f "  %Mid
            print "Raw + Compensation: " + str(Tru)
            print "Expected: " + str(float(Expect)-30)
            
##            if (y > 4) or (minimum < -4):
##                print "Failed"
##            else:
##                print "Passed"

        else:
            median = test1
            print "Maximum: %f" %SWRV
##            if (maximum > -10):
##                print "Failed"
##            else:
##                print "Passed"
                
##        )

        reply = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            ok = True
            print "Yes"
        if reply[0] == 'n':
            ok = False
            print "NO"
    

    #setting the save directory to internal memory
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))

    #setting the format to save a screenshot
    N9918A.values_format.is_binary = True
    N9918A.values_format.datatype = 'B'
    N9918A.values_format.is_big_endian = False
    N9918A.values_format.container = bytearray

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")

    #creating a file with the product number to write the image data to it
    save_dir = open(pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

    #output data to Text File
    outputFile = open(fileName+".txt","a")
    outputFile.write(pictureFileName)
    outputFile.write("\n")
    outputFile.write("ScreenShot FileName: " + pictureFileName + ".png")
    outputFile.write("\n")
    if (isVSWR == 0):
        CsvFileName = pictureFileName + ".csv"
        outputFile.write("RAW Gain: %s " %Mid)
        outputFile.write("\n")
        outputFile.write("Expected: %s " %Expect)
        outputFile.write("\n")
        outputFile.write("RAW + Compensation: %s" %Tru)
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
    else:
        outputFile.write("Maximum (worst) Input Return Loss(VSWR): %s " %SWRV )
        outputFile.write("\n")

    #outputFile.write("-----------------------------------")
    outputFile.write("\n")
    outputFile.write("\n")
    
    outputFile.close
    

take_screenshot()


