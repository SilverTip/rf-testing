## Federico / Doug Forsythe Orbital Research Ltd.
##-----------------------------------------------------------
import serial, os, time ,csv
import pyvisa as visa
##import visa
import time
from datetime import datetime
from string import upper
##-----------------------------------------------------------
## From various code from Federico
##2020-07-13 thus begins the rewrite for Ka 1:1
##-----------------------------------------------------------


##===========================================================================
## FUNCTIONs    
##===========================================================================    
def MSG_LOG(msg_string):
    if IS_LOG == 1:
        logfile.write(msg_string + "\r")
    print (msg_string )
    return

def timestamp (msg = ""):
    ts = time.asctime( time.localtime(time.time())) + " : " + msg
    print ts
    return ts

def pause_anykey():
    if SilentRunning != 1:
        demo = raw_input("paused - hit any key to continue")
    return

def setup_date():
    global RO_offset, DATE1, DATE2, DATE3, UTC
#capture date time parameters
    is_dst = time.daylight and time.localtime().tm_isdst > 0
    utc_offset = - (time.altzone if is_dst else time.timezone)
    RO_offset = str(utc_offset/36)
    UTC  = int(time.time())
    DATE1  =  time.strftime("%Y%m%d,%H%M%S", time.localtime(time.time()))
    DATE2  =  time.strftime("%Y%m%d-%H%M%S", time.localtime(time.time()))
    DATE3  =  time.strftime("(%Y-%m-%d)%Hh%Mm", time.localtime(time.time()))
    return

def isWritable(path):
    import tempfile
    import errno
    try:
        testfile = tempfile.TemporaryFile(dir = path)
        testfile.close()
    except OSError as e:
        if e.errno == errno.EACCES:  # 13
            return False;
        e.filename = path
        raise
    return True;

def setup_logfile():
    global IS_LOG
    global CID, UID, ODU_SN
    global logfilename, subdir_string
    global logfile
    if IS_LOG == 0:  
##----------- following from  function CIDUID()       
        UID = ""
        CID = ""
        if SilentRunning != 1:
            CID = raw_input("Enter 4 digit CID# / docket# (%s): " %(defaultCID))
            UID = raw_input("Enter 3 digit UID# (%s): " %(defaultUID))
        UID_LEN = len(UID)
        if CID == "":
            CID = defaultCID
        elif len(CID) < 4:
            CID = "000" + CID
        CID = CID[-4:]
        if UID == "":
            UID = str(101)
        elif UID_LEN == 1:
            UID = "00" + UID
        elif UID_LEN == 2:
            UID = "0" + UID
        elif UID_LEN > 3:
            UID = UID[-3:]
        ODU_SN = CID+ "-"+ UID
        print ('using ODU: '+ ODU_SN)
##-----------
        if os.access(os.getcwd(), os.W_OK) is True:
            print ("\nLocal DIR (%s) is writable" %(os.getcwd()))        
            IS_LOG = 1
            subdir_string = "ODU "+ ODU_SN+ " Test Data\\"
##            subdir_string = CID + "-" + UID + "-ScriptData\\"		  # 2 level
            if not os.path.exists(subdir_string):
                os.makedirs(subdir_string)
            print ("output subdirectory is: %s" %(subdir_string))
            if UTC == 0:
                setup_date()

            Testmode = ODU_SN + ' ODU1'+ CFG +'-'+RFband + 'Band-' + PLL+ '-'+ LO+ 'LO RF Test'
            logfilename = subdir_string+ Testmode+ " ("+ DATE3 + ").txt"
            if os.path.isfile(logfilename):
                logfile = open(logfilename, 'a')
                print ("append to file: " + logfilename)
            else:
                logfile = open(logfilename, 'w')
                print ("created file: " + logfilename)
            print ("Test started at: %s\r\n" %(DATE3))
            logfile.write(APPtitle + " Ver:" + APPver + " Test started at: "+ DATE3 +"\n")
            logfile.write(Testmode +' (UNIT: ' +LNBmodel + ")\r\n")
            print (Testmode +' (UNIT: ' +LNBmodel + ")\r\n")
        else:
            print ("\n\rLocal DIR (%s) is NOT writable\r\n" %(os.getcwd()))
            IS_LOG = 0
    elif IS_LOG== 2:
        logfile = open(logfilename, 'a')
        print ("append to file: " + logfilename)
##        print (os.path.isfile(logfilename))
    return

def close_logfile(LFILE,option):
    global IS_LOG
    if IS_LOG==1:
        if option==0:
            LFILE.write("\r\nEND OF FILE\r\n")
            IS_LOG = 0
        else:
            IS_LOG = 2       
        LFILE.close()
    return
##===========================================================================
def setupSTAfiles():
    global LO,RFband,BandN,CFG,PLL,LNBmodel,STAlist,SubTestN
    STAext = '.STA'
    RLin = 'VSWR'
    RLout = 'VSWROUT'
    DOList = ['H','L']
    MBList = ['1','2','3','4']

 ##----------------------
    CoaxOUT = 'SO'
    SubTestN = '1'
    UserInput = raw_input('Enter SO, for single output, DO for dual output, MB for multiBand [default:' + CoaxOUT + '] ').upper()
    if UserInput != "":
        CoaxOUT = UserInput
    if CoaxOUT != 'DO':
        if CoaxOUT != 'MB':
            CoaxOUT = 'SO'  
            BandN = 'H'     ##default
            SubTestN = '1'  ##default
    print CoaxOUT

    if CoaxOUT == 'DO':
        SubTestN = '1'
        BandN = 'H'
        UserInput = raw_input('Enter RFBand(H,L):[default:'+ BandN + '] ').upper()
        if UserInput != "":
            BandN = UserInput
        if BandN != 'H':
            BandN = 'L'
            SubTestN = '5'
    elif CoaxOUT == 'MB':
        BandN = '1'
        SubTestN = '1'
        UserInput = raw_input('Enter RFBand(1,2,3,4):[default:'+ BandN + '] ').upper()
        for UserInput in MBList:
            BandN = UserInput
            SubTestN = UserInput
    print BandN, SubTestN
 ##----------------------
    LO = '0000'
    LNBmodel = 'LNB:tba'
    RFband = 'Ku'
    UserInput = raw_input('Enter LNA/LNB model:['+ LNBmodel + '] ').upper()
    if UserInput != "":
        LNBmodel = UserInput
    print LNBmodel
    
    UserInput = raw_input('Enter RF Band:[default:'+ RFband + '] ').upper()
    if UserInput != "":
        RFband = UserInput
    print RFband
 
    UserInput = raw_input('Enter 4 digit LO Freq (GHz x100) Band:[default:' + LO + '] ').upper()
    if UserInput != "":
        LO = UserInput
    if len(LO) < 4:
        LO = '0000' + LO
        LO = LO[-4:]
    print LO
    
    PLL = 'IER'
    if LO != '0000':
        UserInput = raw_input('Enter IER for internal PLL / LNA or XER for external REF:[default:' + PLL + ']' ).upper()
        if UserInput != "":
            PLL = UserInput
            if PLL != 'IER':
                PLL = 'XER'
    print PLL
##----------------------
    CFG = '1'
    UserInput = raw_input('Enter:\n1 for 1:1,\n2 for 1:2,\nNR for non-redundant\n[default is 1:11: ' + CFG + ']').upper()
    if UserInput != "":
        CFG = UserInput 
    if CFG != '2':
        if CFG != 'NR':
            CFG = '1'
    print CFG
##----------------------          
    
    STAarray =[]
    STAprefix = ('R-' + RFband + 'BAND' + LO).upper()
    STAlist = (str(STAprefix + STAext),str(STAprefix + RLin + STAext),str(STAprefix + RLout + STAext),' 10MREF.STA')
    print ('using: '+ str(STAlist) + '\n')
    return()
##---------------------------------------------------------------------------
def savePNG(pictureFileName):   
#setting the format to save a screenshot
        N9918A.values_format.is_binary = True
        N9918A.values_format.datatype = 'B'
        N9918A.values_format.is_big_endian = False
        N9918A.values_format.container = bytearray
#setting the save directory to internal memory
        N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
#saving a screenshot on the equipment                                 
        N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
        print ("saved image internally")
#fetching the image data in an array
        fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")
##        fetch_image_data = N9918A.query_binary_values("MMEM:DATA? 'TESTZ.PNG',datatype='B',is_big_endian=False")
##        fetch_image_data = N9918A.query_binary_values("MMEM:DATA? 'TESTZ.PNG',datatype='B',is_big_endian=False,container = bytearray,chunk_size = 20*1024")
        print ("fetched")
#creating a file with the product number to write the image data to it
        save_dir = open(subdir_string + pictureFileName+".PNG", 'wb')
        save_dir.write(fetch_image_data)
        save_dir.close()

#deleting the image on the equipment
        N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
        N9918A.write("*CLS")
        setup_logfile()
        logfile.write(pictureFileName)
        logfile.write("\n")
        logfile.write("ScreenShot FileName: " + pictureFileName + ".png\n")
        logfile.write("-----------------------------------------\n\n")
        close_logfile(logfile,2)
        return
##---------------------------------------------------------------------------
def REFloss():
    ## need code to recall 10M sta and save png
    ## need code to find mark at peak F and level
    if (PLL == 'XER'):
        TestNum = "(Test 1.7.x) 10Mz REF level"
        print ("\n---------------------\n" + TestNum)
        userinput = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
        if userinput == 'y':
            print ('Yes')
            allPath_done = False
            TESTdata_saved = False
            REF_in_saved = False
            setPath = 'A'
            TestPrefix = '1.7.'
            setLNB = '1'
            lastLNB = '2'
            setInput = 'J3'
            setCable ='CO-1'
            if (CFG == '2'):
                lastLNB = '3'
                TestPrefix = '2.11.'
                setInput = 'J4'
            N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
            N9918A.write(':MMEMory:LOAD:STATe "%s"' % (STAlist[3]))
            N9918A.write(":DISPlay:WINDow:TRACe:Y:SCALe:RLEVel 0")
            N9918A.write(":DISPlay:WINDow:TRACe:Y:SCALe:PDIVision 2")
            N9918A.write(":DISPlay:WINDow:TRACe:Y:SCALe:RPOSition 4")
        else:
            print ('No')
            allPath_done = True
                     
        while (not allPath_done):
##----------
            if (not REF_in_saved):
                TestNum = "(Test 1.7.x) 10Mz REF level input"
                pictureFileName = ODU_SN + " " + TestNum
                N9918A.write('INIT:CONT 1;*OPC?') 
                print ("\n---------------------\n" + TestNum)
                print ("Connect Fieldfox SA mode to IF-OUT Port("+ setInput+ ")")
                print ("Fc=10MHz,span=250Khz")
                userinput = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
                if userinput == 'y':    
                    while (not TESTdata_saved):
    #activating marker 1
                        N9918A.write(':CALCulate:MARKer:ACTivate')
    ##        #setting marker 1 to max
                        N9918A.write(':CALCulate:MARKer:FUNCtion:MAXimum')

    ##        #fetching marker 1 value
                        maximum = float(N9918A.query(':CALCulate:MARKer:Y?'))
                        print maximum
                        print ('Maximum: %5.2f' % maximum)
                        userinput = str(raw_input("Do you wish to use REF_IN: %5.2f (y/n): " % maximum)).lower().strip()
                        if userinput == 'y':
                            REF_in = round(maximum,2)
                        else:
                            while True:               
                                try:
                                    UserInput = raw_input("REF level at " +setInput + " ?")
                                    if UserInput == '':
                                        UserInput = 0
                                    REF_in = float(UserInput)
                                except ValueError:
                                    print ("enter valid number")
                                else:
                                    break
                        REF_in_saved = True
                        print ("REF Level Input: %5.2f dBm" % REF_in)
                        userinput = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
                        if userinput == 'y':
    ##                        N9918A.write('INIT:CONT 0;*OPC?') 
                            TESTdata_saved = True
                            print "Yes"
                            setup_logfile()
                            logfile.write("-----------------------------------------\n" + TestNum + "\n")
                            logfile.write ("REF Level Input: " + str(REF_in) + " dBm at "+ setInput+ "\n")
                            close_logfile(logfile,2)                        
                            savePNG(pictureFileName) 
                        else:
                            TESTdata_saved = False
                            print "NO"
            TESTdata_saved = False        
##----------
            TestNum = "(Test "+ TestPrefix+ setLNB+ ") 10Mz at LNB" + setLNB+ " from Port("+ setInput+ ") Path" +setPath+ " Band" + str(BandN)
            pictureFileName = ODU_SN + " " + TestNum
            N9918A.write('INIT:CONT 1;*OPC?') 
            print ("\n---------------------\n" + TestNum)
            print ("Use Fieldfox SA mode to measure 10MHz level at Cable " + setCable+ " to LNB"+ setLNB)
            print ("Connect Fieldfox SA mode to Cable " + setCable+ " to LNB"+ setLNB)
            print ("Fc=10MHz,span=250Khz")
            userinput = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
            if userinput == 'y':    
                while (not TESTdata_saved):
#activating marker 1
                    N9918A.write(':CALCulate:MARKer:ACTivate')
##        #setting marker 1 to max
                    N9918A.write(':CALCulate:MARKer:FUNCtion:MAXimum')

##        #fetching marker 1 value
                    maximum = float(N9918A.query(':CALCulate:MARKer:Y?'))
                    print maximum
                    print ('Maximum: %5.2f' % maximum)
                    userinput = str(raw_input("Do you wish to use REF level: %5.2f (y/n): " % maximum)).lower().strip()
                    if userinput == 'y':
                        REF_out = round(maximum,2)
                    else:
                        while True:               
                            try:
                                UserInput = raw_input("REF level at Cable " + setCable+ " to LNB"+ setLNB+ "?")
                                if UserInput == '':
                                    UserInput = 0
                                REF_out = float(UserInput)
                            except ValueError:
                                print ("enter valid number")
                            else:
                                break
                    print ("REF out Level: " + str(REF_out) + "dBm")
                    REF_loss = REF_in - REF_out
                    print ("REF Level Input: " + str(REF_out) + "dBm at "+ setCable + " for LNB"+ setLNB)
                    print ("REF Level Loss: " + str(REF_loss) + "dB from "+ setInput + " to "+ setCable + " for LNB"+ setLNB)
                    userinput = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
                    if userinput == 'y':
                        TESTdata_saved = True
                        print "Yes"
                        setup_logfile()
                        logfile.write("-----------------------------------------\n" + TestNum + "\n")
                        logfile.write ("REF Level Input: " + str(REF_in) + " dBm at "+ setInput+ "\n")
                        logfile.write ("REF Level Input: " + str(REF_out) + "dBm at "+ setCable + " for LNB"+ setLNB+ "\n")
                        logfile.write ("REF Level Loss: " + str(REF_loss) + "dB from "+ setInput + " to "+ setCable + " for LNB"+ setLNB+ "\n")
##                        logfile.write ("REF Level Input: " + str(REF_in) + " Peak at dBm at %5.2f \n" % maximum)
                        close_logfile(logfile,2)                   
                        savePNG(pictureFileName) 
                    else:
                        TESTdata_saved = False
                        print "NO"  
            if (lastLNB == setLNB):
                allPath_done = True
            else:              
                TESTdata_saved = False
                setPath = 'B'
                setLNB = '2'
                setInput = 'J3'
                setCable ='CO-3'            
    return()
##---------------------------------------------------------------------------
# Define Error Check Function
def Errcheck():
    myError = []
    ErrorList = N9918A.query("SYST:ERR?").split(',')
    Error = ErrorList[0]
    if int(Error) == 0:
        myError.append('0')
        myError.append("No error")
    else:
     while int(Error)!=0:
        print ("Error #: " + ErrorList[0])
        print ("Error Description: " + ErrorList[1])
        myError.append(ErrorList[0])
        myError.append(ErrorList[1])
        ErrorList = N9918A.query("SYST:ERR?").split(',')
        Error = ErrorList[0]
        myError = list(myError)
    return myError
##---------------------------------------------------------------------------
def setupFOX():
    ### need exception when FFox not Found else program crashes!!!
    global rm,N9918A,STAlist
    SilentFF = True
    defaultIP = "10.0.10.249"
    defaultFF = "MY53101679"
    altFoxSN = "MY53101680"
    IPaddress = ""
    FoxSN = 'n/a'
    userinput = raw_input("FieldFox N9918A SN:[%s][0 to skip]: " %(defaultFF))
    if userinput == "":
        FoxSN = defaultFF
    elif (userinput != '0'):
        FoxSN = userinput
    if (FoxSN != 'n/a'):  
        IPaddress = raw_input("Enter server IP address (local PC) xxx.xxx.xxx.xxx(%s): " %(defaultIP))
        if IPaddress == "":
            IPaddress = defaultIP
        print ("using N9918A sn: " + FoxSN + " IP: " + IPaddress)
        rm = visa.ResourceManager()
        N9918A = rm.open_resource('TCPIP0::' +  str(IPaddress) + '::inst0::INSTR')
        N9918A.timeout = 50000
# Clear the event status registers and empty the error queue
        N9918A.write("*CLS")
# Query identification string *IDN?
        list2 = N9918A.query("*IDN?")
        print (list2)
##        N9918A.query("*IDN?")
##        print (N9918A.read())
# Call and print error check results
        print (Errcheck())
# Query capability
##        print(N9918A.query(':Inst:Cat?'))

    setup_logfile()
    logfile.write("-----------------------------------------")
    logfile.write("\n")
    logfile.write("RF Test Report for " + str(ODU_SN)+'\n')
    logfile.write("Measurements Done Using: FieldFox N9918A sn:" + str(FoxSN)+ " IP: " + IPaddress+'\n')
    logfile.write("Measurements Performed by: " + str(user_name)+'\n')
    logfile.write("Measurements Performed: "  + DATE3 +'\n\n')
    logfile.write("Setup files used: "  + str(STAlist[0])+" "+str(STAlist[1])+" "+str(STAlist[2])+ str(STAlist[3])+"\n\n")     
    print("-----------------------------------------")
    print("\n")
    print("RF Test Report for " + str(ODU_SN)+'\n')
    print("Measurements Done Using: FieldFox N9918A sn:" + str(FoxSN)+'\n')
    print("Measurements Performed by: " + str(user_name)+'\n')
    print("Measurements Performed: "  + DATE3 +'\n\n')
    close_logfile(logfile,2)
                           
    return ##logfile closed
##===========================================================================
def RUN_ODU11():
    global STAlist,logfile
    setupSTAfiles()
    setupFOX()
    REFloss()
    
    defaultPwrCal = -10
    PwrCal = raw_input("What is the Calibrated Power Level?[%s]" %(defaultPwrCal))
    if PwrCal == "":
        PwrCal = defaultPwrCal
    print (PwrCal)
    
    defaultComp = 30
    Comp = raw_input("What is the Compensation value?[%s]" %(defaultComp))
    if Comp == "":
        Comp = defaultComp
    print (Comp)
    

    defaultExpect = 60
    Expect = raw_input("What is the Expected Value?[%s]" %(defaultExpect))
    if Expect == "":
        Expect = defaultExpect
    print (Expect)
    
    InputLevel = 0
##--------------------------
    setup_logfile()
    logfile.write("Instrument Calibrated at -10dBm output with a 20dB Attenuator"+'\n')
    logfile.write("Source Power is then set to -40dBm, therefore all RAW measurements require an offset compensation of 30dB."+'\n')
    logfile.write("This is done to prevent instrument saturation"+'\n')
    logfile.write("LNBs have an expected gain: " + str(Expect)+'\n')
    logfile.write("-----------------------------------------\n\n")
##    logfile.close()
    close_logfile(logfile,2)

    print("Instrument Calibrated at -10dBm output with a 20dB Attenuator"+'\n')
    print("Source Power is then set to -40dBm, therefore all RAW measurements require an offset compensation of 30dB."+'\n')
    print("This is done to prevent instrument saturation"+'\n')
    print("LNBs have an expected gain: " + str(Expect)+'\n')
    print("-----------------------------------------\n\n")
##==========================
##START TESTS    
##--------------------------
    TestNum = "(Test 1.1."+ SubTestN+ ")LNA-LNB1 Active Path Gain Vertical Band" + str(BandN)
    isVSWR = 0    
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "%s"' % (STAlist[0]))
    print ("\n---------------------\n" + TestNum)
    print ("Ensure 20dB attn in place and connect FF PORT1 to Online RF-IN and Power meter to IF-OUT (J3)")
    print ("\nSWITCH TO PATH A")
    print ("Remember to activate {RUN} on field FOX before running test below.")
    time.sleep(1)
    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##--------------------------
    TestNum = "(Test 1.2."+ SubTestN+ ")LNA-LNB2 Active Path Gain Vertical Band" + str(BandN)
    isVSWR = 0
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "%s"' % (STAlist[0]))
    print ("\n---------------------\n" + TestNum)
    print ("Ensure 20dB attn in place and connect FF PORT1 to Online RF-IN and Power meter to IF-OUT (J3)")
    print ("\nSWITCH TO PATH B")
    print ("Remember to activate {RUN} on field FOX before running test below.")
    time.sleep(1)
    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##--------------------------
## Return Loss
    if CFG == '1':       
        TestNum = "(Test 1.6.1) Input Return Loss at ON-line RF-IN Vertical"
        isVSWR = 1
        N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
        N9918A.write(':MMEMory:LOAD:STATe "%s"' % (STAlist[1]))
        print ("\n---------------------\n" + TestNum)
        print ("Ensure 20dB attn is removed and connect to FF PORT1 to to Online RF-IN")
        print ("\nSWITCH TO PATH A")
        print ("Remember to activate {RUN} on field FOX before running test below.")
        time.sleep(1)
        testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
        if testYes[0] == 'y':
            activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##--------------------------
## Return Loss2
    if CFG == 'NR':
        TestNum = "(Test 1.6.1.1) Input Return Loss at ON-line RF-IN POLA"
        isVSWR = 1
        N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
        N9918A.write(':MMEMory:LOAD:STATe "%s"' % (STAlist[1]))
        print ("\n---------------------\n" + TestNum)
        print ("Ensure 20dB attn is removed and connect to FF PORT1 to to Online RF-IN")
        print ("Remember to activate {RUN} on field FOX before running test below.")
        time.sleep(1)
        testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
        if testYes[0] == 'y':
            activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##--------------------------
        TestNum = "(Test 1.6.1.2) Input Return Loss at ON-line RF-IN POLB"
        isVSWR = 1
        N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
        N9918A.write(':MMEMory:LOAD:STATe "%s"' % (STAlist[1]))
        print ("\n---------------------\n" + TestNum)
        print ("Ensure 20dB attn is removed and connect to FF PORT1 to to Online RF-IN")
        print ("Remember to activate {RUN} on field FOX before running test below.")
        time.sleep(1)
        testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
        if testYes[0] == 'y':
            activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)    
##--------------------------        
    TestNum = "(Test 1.6.0.1) Output Return Loss at IF-OUT Port(J3) PathA"
    isVSWR = 1
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "%s"' % (STAlist[2]))
    print ("\n---------------------\n" + TestNum)
    print ("Ensure 20dB attn is removed and connect to FF PORT1 to IF-OUT (J3)")
    print ("\nSWITCH TO PATH A")
    print ("Remember to activate {RUN} on field FOX before running test below.")
    time.sleep(1)
    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##--------------------------
    TestNum = "(Test 1.6.0.2) Output Return Loss at IF-OUT Port (J3) PathB"
    isVSWR = 1
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "%s"' % (STAlist[2]))
    print ("\n---------------------\n" + TestNum)
    print ("Ensure 20dB attn is removed and connect to FF PORT1 to IF-OUT (J3)")
    print ("\nSWITCH TO PATH B")
    print ("Remember to activate {RUN} on field FOX before running test below.")
    time.sleep(1)
    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect)
##--------------------------
    N9918A.close()
    rm.close()
    setup_logfile()
    close_logfile(logfile,0)
    return


##===========================================================================

def activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect):

    ok= False

    while (ok == False):
        pictureFileName = ODU_SN + " " + TestNum
        #activating marker 1
        N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
##        #setting marker 1 to max
        N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
##        #fetching marker 1 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        test1 = float(maximum[0]) + 31.8
        SWRV = float(maximum[0])
        



        #activating marker 2
        N9918A.write(':CALCulate:SELected:MARKer2:ACTivate')
##        #setting marker 2 to max
        N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
##        #fetching marker 2 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        test2 = minimum[0] + 31.8
        y = (test1 - test2)/2
        
        
        if (isVSWR == 0):
            Mid = test2 + y
            Tru = float(Comp) + Mid
            print "minimum: %f " %test2
            print "maximum: %f" %test1
            print "Raw: %f "  %Mid
            print "Raw + Compensation: " + str(Tru)
            print "Expected: " + str(Expect)
            
##            if (y > 4) or (minimum < -4):
##                print "Failed"
##            else:
##                print "Passed"

        else:
            median = test1
            print "Maximum: %f" %SWRV
##            if (maximum > -10):
##                print "Failed"
##            else:
##                print "Passed"
                
##        )

        reply = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            ok = True
            print "Yes"
        if reply[0] == 'n':
            ok = False
            print "NO"
    

    #setting the save directory to internal memory
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    
    

    #setting the format to save a screenshot
    N9918A.values_format.is_binary = True
    N9918A.values_format.datatype = 'B'
    N9918A.values_format.is_big_endian = False
    N9918A.values_format.container = bytearray

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")

    #creating a file with the product number to write the image data to it
    save_dir = open(subdir_string + pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

    #output data to Text File
    setup_logfile()
##    logfile = open(logfilename+".txt","a")
    logfile.write(pictureFileName)
    logfile.write("\n")
    logfile.write("ScreenShot FileName: " + pictureFileName + ".png")
    logfile.write("\n")
    
    if (isVSWR == 0):
        CsvFileName = pictureFileName + ".csv"
        logfile.write("RAW Gain: %s " %Mid)
        logfile.write("\n")
        logfile.write("Expected: %s " %Expect)
        logfile.write("\n")
        logfile.write("RAW + Compensation: %s" %Tru)
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
    else:
        CsvFileName = pictureFileName + "CT.csv"
        SPFileName = pictureFileName + ".s2p"
        logfile.write("Maximum (worst) Input Return Loss(VSWR): %s " %SWRV )
        logfile.write("\n")
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
        N9918A.write(":MMEMory:STORe:SNP '%s'" %SPFileName)

    logfile.write("\n-----------------------------------------\n\n")
    close_logfile(logfile,2)
    return
##===========================================================================
def activateMaxMinM20C(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect):

    ok= False

    while (ok == False):
        pictureFileName = ODU_SN + " " + TestNum
        #activating marker 1
##        N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
##        #setting marker 1 to max
        N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
##        #fetching marker 1 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        test1 = float(maximum[0]) + 31.8
        SWRV = float(maximum[0])  

        #activating marker 2
##        N9918A.write(':CALCulate:SELected:MARKer2:ACTivate')
##        #setting marker 2 to max
        N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
##        #fetching marker 2 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        test2 = minimum[0] + 31.8
        y = (test1 - test2)/2
        
        
        if (isVSWR == 0):
            Mid = test2 + y
            Tru = float(Comp) + Mid
            print "minimum: %f " %test2
            print "maximum: %f" %test1
            print "Raw: %f "  %Mid
            print "Raw + Compensation: " + str(Tru)
            print "Expected: " + str((int(Expect) - int(20)))
            
##            if (y > 4) or (minimum < -4):
##                print "Failed"
##            else:
##                print "Passed"

        else:
            median = test1
            print "Maximum: %f" %SWRV
##            if (maximum > -10):
##                print "Failed"
##            else:
##                print "Passed"
                
##        )

        reply = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            ok = True
            print "Yes"
        if reply[0] == 'n':
            ok = False
            print "NO"
    

    #setting the save directory to internal memory
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))

    #setting the format to save a screenshot
    N9918A.values_format.is_binary = True
    N9918A.values_format.datatype = 'B'
    N9918A.values_format.is_big_endian = False
    N9918A.values_format.container = bytearray

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")

    #creating a file with the product number to write the image data to it
    save_dir = open(subdir_string + pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

    #output data to Text File
    setup_logfile()
##    logfile = open(logfilename+".txt","a")
    logfile.write(pictureFileName)
    logfile.write("\n")
    logfile.write("ScreenShot FileName: " + pictureFileName + ".png")
    logfile.write("\n")
    if (isVSWR == 0):
        CsvFileName = pictureFileName + ".csv"
        logfile.write("RAW Gain: %s " %Mid)
        logfile.write("\n")
        logfile.write("Expected: %s " %Expect)
        logfile.write("\n")
        logfile.write("RAW + Compensation: %s" %Tru)
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
    else:
        logfile.write("Maximum (worst) Input Return Loss(VSWR): %s " %SWRV )
    logfile.write("\n-----------------------------------------\n\n")
    close_logfile(logfile,2)
    return
##===========================================================================
def activateMaxMinM30C(N9918A,InputLevel,ODU_SN,TestNum,logfilename,isVSWR,PwrCal,Comp,Expect):

    ok= False

    while (ok == False):
        pictureFileName = ODU_SN + " " + TestNum
        #activating marker 1
##        N9918A.write(':CALCulate:SELected:MARKer1:ACTivate')
##        #setting marker 1 to max
        N9918A.write(':CALCulate:SELected:MARKer1:FUNCtion:MAXimum')
##        #fetching marker 1 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        maximum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer1:Y?')
        test1 = float(maximum[0]) + 31.8
        SWRV = float(maximum[0])
        



        #activating marker 2
##        N9918A.write(':CALCulate:SELected:MARKer2:ACTivate')
##        #setting marker 2 to max
        N9918A.write(':CALCulate:SELected:MARKer2:FUNCtion:MINimum')
##        #fetching marker 2 value
##        temp_values = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        minimum = N9918A.query_ascii_values(':CALCulate:SELected:MARKer2:Y?')
        test2 = minimum[0] + 31.8
        y = (test1 - test2)/2
        
        
        if (isVSWR == 0):
            Mid = test2 + y
            Tru = Mid
            print "minimum: %f " %test2
            print "maximum: %f" %test1
            print "Raw: %f "  %Mid
            print "Raw + Compensation: " + str(Tru)
            print "Expected: " + str(float(Expect)-30)
            
##            if (y > 4) or (minimum < -4):
##                print "Failed"
##            else:
##                print "Passed"

        else:
            median = test1
            print "Maximum: %f" %SWRV
##            if (maximum > -10):
##                print "Failed"
##            else:
##                print "Passed"
                
##        )

        reply = str(raw_input("Do you wish to save data?"+' (y/n): ')).lower().strip()
        if reply[0] == 'y':
            ok = True
            print "Yes"
        if reply[0] == 'n':
            ok = False
            print "NO"
    

    #setting the save directory to internal memory
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))

    #setting the format to save a screenshot
    N9918A.values_format.is_binary = True
    N9918A.values_format.datatype = 'B'
    N9918A.values_format.is_big_endian = False
    N9918A.values_format.container = bytearray

    #saving a screenshot on the equipment                                 
    N9918A.write(":MMEMory:STORe:IMAGe 'TESTZ'")
    #fetching the image data in an array
    fetch_image_data = N9918A.query_values("MMEM:DATA? 'TESTZ.PNG'")

    #creating a file with the product number to write the image data to it
    save_dir = open(subdir_string + pictureFileName+".PNG", 'wb')
    save_dir.write(fetch_image_data)
    save_dir.close()

    #deleting the image on the equipment
    N9918A.write(":MMEM:DEL 'TESTZ.PNG'")
    N9918A.write("*CLS")

    #output data to Text File
    setup_logfile()
##    logfile = open(logfilename+".txt","a")
    logfile.write(pictureFileName)
    logfile.write("\n")
    logfile.write("ScreenShot FileName: " + pictureFileName + ".png")
    logfile.write("\n")
    if (isVSWR == 0):
        CsvFileName = pictureFileName + ".csv"
        logfile.write("RAW Gain: %s " %Mid)
        logfile.write("\n")
        logfile.write("Expected: %s " %Expect)
        logfile.write("\n")
        logfile.write("RAW + Compensation: %s" %Tru)
        N9918A.write(":MMEMory:STORe:FDATa '%s'" %CsvFileName)
    else:
        logfile.write("Maximum (worst) Input Return Loss(VSWR): %s " %SWRV )

    logfile.write("\n-----------------------------------------\n\n")
    close_logfile(logfile,2)
    return
##---------------------------------------------------------------------------------------
## END of FUNCTIONs
#================================================================================
SilentRunning = 0 ##1 = use defaults and no pause for key 0= normal
#Strings and setup basic commands
##setuo non-intialized values
IS_SER = 0      
IS_LOG = 0      
IS_STA = 0      
UTC = 0         
defaultCID = '7569'    
defaultUID = '102'    

      
default_com = '6'
##host = '192.168.1.96'
##host = '192.168.1.199'
host = '10.0.10.96'
gateway = '10.0.10.1'
mask = '255.255.255.0'
##===========================================================================    
##MAIN CODE follows
##===========================================================================
##IS_H = 0
IS_H = 1
IS_notDONE = 1
APPtitle="ODU RF TEST"
APPver = str('(' + os.path.basename(__file__)) + ') Modified: ' + str(datetime.fromtimestamp(int(os.path.getmtime(__file__))))
print ("%s,%s" %(APPtitle, APPver))

user_name = "code test"
if SilentRunning != 1:
    userinput = raw_input("Enter Operator Name: ").upper()
    if userinput != "":
        user_name = userinput
    print user_name    
        
while (IS_notDONE):
    if IS_H == 1 :

        print ("\r")
##        print ("Ka       - run Ka 1:1 IER LO=20.25GHz\r")
        print ("ODU11    - ODU 1:1 RF test\r")
        print ("ODU12    - ODU 1:2 RF test\r")        
        print ("FF       - setup/ test field fox\r")
        print ("REF      - measure REF levels\r")                    
        print ("S11      - input return LOSS\r")
        print ("S22      - output return LOSS\r")        
        print ("S21      - Path Gain\r")       
        print ("STA      - list STA files\r")
        print ("LS       - List Log\r")
        print ("H        - Toggle menu option\r")
        print ("Q        - QUIT\r\n")

    userinput = raw_input(" Enter mode,type H for help: (default is Q): ").upper()

    if userinput != "":
        mode = userinput
        setup_date()
    else:
        mode = "Q"
    print ("Using %s mode\r\n\n" %(mode))
#******************************************
    if mode == "Q":
        break
#******************************************
    elif mode == "STA":
        setupSTAfiles()    
#******************************************
    elif mode == "H":
        if IS_H == 1:
             IS_H = 0
        else:
             IS_H = 1
#******************************************
    elif mode == "REF":
        mode="FF-REF"
        setupSTAfiles()
        setupFOX()
        REFloss()
#******************************************  
    elif mode == "FF":
        mode="FF-RF"
        setupFOX()  
#******************************************             
    elif mode == "ODU11":
        mode="ODU11-RF"
        IS_notDONE = 0
        RUN_ODU11()               
#******************************************
##    elif mode == "KA":
##        mode="ODU11-Ka-IER-LO2025"
##        IS_notDONE = 0
##        RUN_ODU11_KA2025()               
#******************************************                      
    elif mode == "LS":
        import serial.tools.list_ports
        ports = serial.tools.list_ports.comports()
        for port, desc, hwid in sorted(ports):
            print("{}: {} [{}]".format(port, desc, hwid))

        if IS_LOG == 1:
            print ("logfile: %s" %(logfile.name))
        else:
             print ("Log file closed.\r\n")
        if IS_SER  == 1:
             print ("Using serial port COM%s\r\n" %(com_port))
        else:
             print ("Serial port closed.\r\n")
        pause_anykey()
#******************************************
    else:
        print ("mode not defined\n")
        break
#****************
if IS_LOG != 0:
    close_logfile(logfile,0)
##    logfile.close()
if IS_SER != 0:
    ser.close()
##===========================================================================
## END OF MAIN CODE 





