
    #(Test 2.1) LNB-LNB1 Active Path Gain (High band) BAND1
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBAND.STA"')
    print('(Test 2.1)LNB-LNB1 Active Path Gain (High band) BAND1, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    #time.sleep(DELAY)
    TestNum = "(Test 2.1)LNB-LNB1 Active Path Gain Vertical BAND1"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test 2.2) LNB-LNB1 Active Coupled Output
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBAND.STA"')
    print('(Test 2.2)LNB-LNB1 Output BAND1 Coupled Output, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    #time.sleep(DELAY)
    TestNum = "(Test 2.2)LNB-LNB1 Vertical Output BAND1 Coupled Output"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

      


    #(Test 2.3)LNB1-LNB1 Active Path using Cross Guide Coupler Input
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBAND.STA"')
    print('(Test 2.3)LNB1-LNB1 Active Path using Cross Guide Coupler Input, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.3)LNB1-LNB1 Vertical Active Path using Cross Guide Coupler Input"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM30C(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test 2.4) LNB2-LNB2 Active Path Gain (High band) BAND2
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBAND.STA"')
    print('(Test 2.4)LNB2-LNB2 Active Path Gain (High band) BAND2, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.4)LNB2-LNB2 Horizontal Active Path Gain (High band) BAND2"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test 2.4) LNB-LNB2 Active Coupled Output
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBAND.STA"')
    print('(Test 2.4)LNB-LNB2 Output BAND1 Coupled Output, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    #time.sleep(DELAY)
    TestNum = "(Test 2.4)LNB-LNB2 Horizontal Output BAND1 Coupled Output"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)
        

    

    #(Test 2.6)LNB1-LNB1 Standby TEST Path
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBAND.STA"')
    print('(Test 2.6)LNB1-LNB1 Standby TEST Path, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.6)LNB1-LNB1 Standby TEST Path"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)    


    #Test 2.7 (Test 2.7) LNB2-LNB2 Active Path using Cross Guide Coupler Input
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBAND.STA"')
    print('(Test 2.7) LNB2-LNB2 Active Path using Cross Guide Coupler Input, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.7) LNB2-LNB2 Horizontal Active Path using Cross Guide Coupler Input"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM30C(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test 2.8) LNB3-LNB3 Active Path Gain (LNB1 offline)
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBAND.STA"')
    print('(Test 2.8) LNB3-LNB3 Active Path Gain (LNB1 offline), press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.8) LNB3-LNB3 Vertical Active Path Gain (LNB1 offline)"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #(Test 2.9) LNB3-LNB3 Active Path Gain (LNB2 offline)
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBAND.STA"')
    print('(Test 2.9) LNB3-LNB3 Active Path Gain (LNB2 offline), press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.9) LNB3-LNB3 Horizontal5 Active Path Gain (LNB2 offline)"
    isVSWR = 0

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)        
        
## Return Loss        

    #(Test 2.10.1) Input Return Loss at ON-line RHCP RF-IN Vertical
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBANDVSWR.STA"')
    print('(Test 2.10.1) Input Return Loss at ON-line RHCP RF-INVertical, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.10.1) Input Return Loss at ON-line RHCP RF-INVertical"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #(Test 2.10.2)Input Return Loss at ON-line LHCP RF-INHorizontal
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBANDVSWR.STA"')
    print('(Test 2.10.2)Input Return Loss at ON-line LHCP RF-INHorizontal, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.10.2)Input Return Loss at ON-line LHCP RF-IN Horizontal"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #(Test 2.10.3) Input Return Loss at OFF-line RF-IN Port
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBANDVSWR.STA"')
    print('(Test 2.10.3) Input Return Loss at OFF-line RF-IN Port, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test 2.10.3) Input Return Loss at OFF-line RF-IN Port"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test2.10.4)Input Return Loss at RHCP Test RF-INVertical
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBANDVSWR.STA"')
    print('(Test2.10.4)Input Return Loss at RHCP Test RF-IN Vertical, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test2.10.4)Input Return Loss at RHCP Test RF-IN Vertical"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #(Test2.10.5)Input Return Loss at LHCP Test RF-INHorizontal
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBANDVSWR.STA"')
    print('(Test2.10.5)Input Return Loss at LHCP Test RF-IN Horizontal, press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test2.10.5)Input Return Loss at LHCP Test RF-IN Horizontal"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)


    #(Test2.10.7)Output Return Loss at IF-OUT Port (J4)
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBANDVSWROUT.STA"')
    print('(Test2.10.7)Output Return Loss at IF-OUT Port (J4), press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test2.10.7)Output Return Loss at IF-OUT Port (J4)"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    #(Test2.10.9)Output Return Loss at IF-OUT Port (J3)
    N9918A.write(':MMEMory:CDIRectory "%s"' % ('[INTERNAL]:'))
    N9918A.write(':MMEMory:LOAD:STATe "R-XBANDVSWROUT.STA"')
    print('(Test2.10.9)Output Return Loss at IF-OUT Port (J3), press Enter')
##    print('Check ULC is on Band 1 and press Enter')
    time.sleep(DELAY)
    TestNum = "(Test2.10.9)Output Return Loss at IF-OUT Port (J3)"
    isVSWR = 1

    testYes = str(raw_input("Do you wish to run this test?"+' (y/n): ')).lower().strip()
    if testYes[0] == 'y':
        activateMaxMinM(N9918A,InputLevel,ODU_SN,TestNum,fileName,isVSWR,PwrCal,Comp,Expect)

    
    N9918A.close()
    rm.close()
